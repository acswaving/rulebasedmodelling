<#import "keen.ftl" as k>

<#macro pageTitle>
    Lookout: Combining Data Sources
</#macro>

<#macro pageHead></#macro>

<#macro pageContent>
    <@k.subheader pagetitle="Data combination builder" level1="Prepare" level2="Recombine blocks"></@k.subheader>

    <!-- begin:: Content -->
    <div class="k-content k-grid__item k-grid__item--fluid" id="k_content">
        <!--begin::Dashboard 1-->
        <div class="row">
            <div class="col-lg-12">

                <div class="k-portlet">
                    <div class="k-portlet__head">
                        <div class="k-portlet__head-label">
                            <h3 class="k-portlet__head-title">
                                Builder
                            </h3>
                        </div>
                        <div class="k-portlet__head-toolbar">
                            <div class="k-portlet__head-group">
                                <button type="button" class="btn btn-primary btn-sm btn-bold btn-upper"  onclick="executeQuery()">Execute</button>
                            </div>
                        </div>
                    </div>
                    <div class="k-portlet__body">
                        <div class="k-portlet__content">
                            <div id="blocklyDiv" style="height: 480px; width: 800px;"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <xml xmlns="http://www.w3.org/1999/xhtml" id="toolbox" style="display: none;">
        <category name="Combine" colour="#5b5ba5">
            <block type="combine">
                <field name="NAME">new table name</field>
            </block>
            <block type="table">
                <field name="TableName">table name</field>
            </block>
            <block type="columns">
                <field name="columnList">column list</field>
            </block>
            <block type="by">
                <field name="joinColumn">column to join on</field>
            </block>
        </category>
        <category name="Filter">
            <block type="filter"></block>
            <block type="condition"></block>
            <block type="and"></block>
        </category>
    </xml>

</#macro>

<#macro pageScript>
    <script src="/assets/vendors/custom/google-blockly-bc210ec/blockly_compressed.js"></script>
    <script src="/assets/vendors/custom/google-blockly-bc210ec/blocks_compressed.js"></script>
    <script src="/assets/vendors/custom/google-blockly-bc210ec/javascript_compressed.js"></script>
    <script src="/assets/vendors/custom/google-blockly-bc210ec/msg/js/en.js"></script>
    <script src="/assets/vendors/custom/google-blockly-bc210ec/blocks/customBlocks.js"></script>
    <script src="/assets/vendors/custom/google-blockly-bc210ec/blocks/combineDataBlocks.js"></script>
    <script>
        var workspace = Blockly.inject('blocklyDiv',
            {
                toolbox: document.getElementById('toolbox'),
                toolboxPosition: 'end',
                horizontalLayout: true,
                scrollbars: false
            }
        );

        executeQuery = function() {
            var code = Blockly.JavaScript.workspaceToCode(Blockly.getMainWorkspace());
            $.get('/api/v1/datacombination/data/info', {
                queryinfo: code
            })
            alert(code)
        }



    </script>


</#macro>