<#import "keen.ftl" as k>

<#macro pageTitle>
    Data Modelling: Combining Data Sources
</#macro>

<#macro pageHead>
    <link href="/assets/vendors/custom/footable/css/footable.bootstrap.css" rel="stylesheet" type="text/css" />

    <style>
        div.horizontal {
            width: 1000px;
            height: 500px;
            overflow: auto;
        }

        div.horizontal_scroll {
            width: 100%;
            height: 300px;
            overflow: auto;
        }

    </style>
</#macro>

<#macro pageContent>

    <@k.subheader pagetitle="Tables" level1="Analyse" level2="Tables"></@k.subheader>


    <!-- begin:: Content -->
    <div class="k-content k-grid__item k-grid__item--fluid" id="k_content">
    <!--begin::Dashboard 1-->
        <div class="row">
            <div class="col-md-4">
                <@k.basicportlet title="Available Tables">
                    <div class="row">
                        <div class="col-md-12">
                            <form>
                                <select name="tables" id="tables" onchange = getTable(this)></select>
                                <br><br>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary btn-sm" onclick="removeTables()">Remove User Created Tables</button>
                        </div>
                    </div>
                </div>
                </@k.basicportlet>
            <div class="col-md-8">
                <@k.toolbarportlet title="Table Statistics" dropdown_name="Summaries">
                    <p id="status"></p>
                </@k.toolbarportlet>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <@k.basicportlet title="Preview Table (First 4 rows)">
                    <div class="horizontal">
                        <p id="preds"></p>
                    </div>
                </@k.basicportlet>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <@k.basicportlet title="Write Table">
                    <form>
                        <select name="tablewrite" id="tablewrite" onchange = writetable(this)></select>
                        <br><br>
                    </form>
                </@k.basicportlet>
            </div>
        </div>

    </div>

</#macro>

<#macro pageScript>
    <script src="/assets/vendors/custom/footable/js/footable.js" type="text/javascript"></script>
    <!-- Code Editor scripts -->
    <script>


        var listGenerator = function(columns){
            var myNode = document.getElementById("mylist");
            while (myNode.firstChild) {
                myNode.removeChild(myNode.firstChild);
            }
            var node = document.createElement("li");                 // Create a <li> node
            node.setAttribute("class", "k-nav__item")
            var anode = document.createElement("a");
            var textnode = document.createTextNode('Overall');         // Create a text node
            anode.appendChild(textnode);
            anode.setAttribute("href", "#");
            anode.setAttribute("id", "overall");
            anode.setAttribute("class", "summary");
            node.appendChild(anode);
            document.getElementById("mylist").appendChild(node);
            for(c in columns) {
                var node = document.createElement("li");                 // Create a <li> node
                node.setAttribute("class", "k-nav__item")
                var anode = document.createElement("a");
                var textnode = document.createTextNode(columns[c]);         // Create a text node
                anode.appendChild(textnode);
                anode.setAttribute("href", "#");
                anode.setAttribute("id", columns[c]);
                anode.setAttribute("class", 'summary');
                node.appendChild(anode);
                document.getElementById("mylist").appendChild(node);
            }
        };


        var tables;
        var columns;
        $.get('/api/v1/datacombination/data/datasetInfo', {}, function(data) {
            tables = Object.keys(data);
            columns = data;
        });


        findTable = function(word, namelist){
            if(word == 'table'){
                for(var i = 0; i < tables.length; i++){
                    namelist.push(tables[i])
                }
                return namelist;
            }
            if(tables.includes(word)){
                var colArray = columns[word];
                for(var i = 0; i < colArray.length; i++){
                    namelist.push(word+"."+colArray[i])
                }
                return namelist;
            }
            if(word!='table' & !tables.includes(word)){
                namelist.push(word + ".");
                return namelist;
            }
            return namelist;
        };


        getDefinitions = function(e) {
            var temp = editor.getValue();
            var $this = $(e);
            $this.button('loading');
            if(temp === ""){
                $this.button('reset');
                buildSelect;
            }
            else {
                $.when($.get('/api/v1/datacombination/data/info', {
                    queryinfo: temp
                })).done(function () {
                    $this.button('reset');
                    buildSelect;
                });
            }
        };


        buildSelect = function(element){
            $.get('/api/v1/datacombination/data/availableTables',{},function(options) {
                    removeOptions(document.getElementById(element));
                    // var option = $('<option>');
                    var o = new Option('select', 'select');
                    // option.attr('select', 'select').text('select');
                    $('#'+ element).append(o);
                    for (var val in options) {
                        var o = new Option(options[val], options[val]);
                        $(o).html(options[val]);
                        // option.attr(options[val], options[val]).text(options[val]);
                        $('#'+element).append(o);
                    }
                }
            )
        }

        removeOptions = function(selectbox)
        {
            var i;
            for(i = selectbox.options.length - 1 ; i >= 0 ; i--)
            {
                selectbox.remove(i);
            }
        }

        var table;

        // getTable = function(selectedObject){
        //     $.get("/api/v1/datacombination/data/viewHeadOfTable",{
        //         tables: selectedObject.value
        //     },function(tableData){
        //         // data table
        //         if(table != undefined) { // Check if table object exists and needs to be flushed
        //             table.destroy(); // For new version use table.destroy();
        //             $('#tabledata').empty(); // empty in case the columns change
        //         }
        //         var colNames = Object.keys(tableData[0]);
        //         var tablelist = [];
        //         colNames.forEach(col => {
        //             let entry = {title: col, data: col, defaultContent: "-"};
        //             tablelist.push(entry);
        //         });
        //         // table = datatable('#tabledata', tablelist, tableData, null, null, true);
        //     });
        // };

        var dataSummary;
        var summary;

        getTable = function(selectedObject){
            $.get("/api/v1/datacombination/data/tableMetadata",{
                tableName: selectedObject.value
            },function(data) {
                $.get("/api/v1/datacombination/data/tableStatistics", {
                    tables: selectedObject.value
                }, function (tableData) {
                    summary = JSON.parse(data[0]['description']);
                    let tabName = selectedObject.value;
                    dataSummary = Object.values(tableData);
                    let cols = [];
                    dataSummary.forEach(d => cols.push(d['name']));
                    summary['table'] = tabName;
                    summary['# of columns'] = dataSummary.length;
                    $('#status').html($.cfi.toTable(summary));
                    listGenerator(cols);
                });
                $.get("/api/v1/datacombination/data/viewHeadOfTable", {
                    tables: selectedObject.value
                }, function (tableData) {
                    $('#preds').html(createTable(tableData));
                    jQuery(function($){
                        $('#predTable').footable();
                    });
                });
            });
        };

        $(document).on('click', 'a.summary', function(e) {
            let colName = e.target.id;
            if(colName != 'overall') {
                let colOfInterest = dataSummary.filter(d => d['name'] === colName);
                $('#status').html($.cfi.toTable(JSON.parse(colOfInterest[0]['description'])))
            }
            else{
                $('#status').html($.cfi.toTable(summary))
            }
        });

        writetable = function(selectedObject){
            $.get("/api/v1/datacombination/data/writetable",{
                table: selectedObject.value
            },function(tableData){})
        }

        removeTables = function(){
            $.get("/api/v1/datacombination/data/removeTables");
            location.reload();
        }



        createTable = function(data){
            const newTable = '<table class="table table-bordered table-condensed table-striped" id="predTable" />';
            let tab;
            tab = $(newTable);

            //create table header from object keys
            const row = $('<tr />');
            $.each( data[0], function( key, value ) {
                row.append($('<th />').append(key));
            });
            tab.append($('<thead />').append(row));

            //create table body from object values
            const bdy = $('<tbody />');
            $.each( data, function( key, value ) {
                const bodyrow = $('<tr />');
                $.each( value, function( k, v) {
                    bodyrow.append($('<td />').append(v));
                });
                bdy.append(bodyrow);
            });
            tab.append(bdy);

            return tab;
        }


        buildSelect('tables');
        buildSelect('tablewrite');


    </script>

</#macro>