<#import "keen.ftl" as k>

<#macro pageTitle>
    Data Modelling: Decision Trees
</#macro>

<#macro pageHead>
    <style>
        #cy {
            width: 100%;
            height: 100%;
            position: absolute;
            left: 0;
            top: 0;
            z-index: 999;
        }

        .multiselect {
            width: 200px;
        }

        .selectBox {
            position: relative;
        }

        .selectBox select {
            width: 100%;
            font-weight: bold;
        }

        .overSelect {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }

        #checkboxes {
            display: none;
            border: 1px #dadada solid;
        }

        #checkboxes label {
            display: block;
        }

        #checkboxes label:hover {
            background-color: #1e90ff;
        }

        .table-scrollable{
            max-width: 100%;
        }



    </style>

<#--<script src="/global/plugins/cytoscape.js"></script>-->
    <script src="/assets/vendors/custom/cytoscape/cytoscape.min.js"></script>
    <script src="/assets/vendors/custom/cytoscape/dagre.js"></script>
    <script src="/assets/vendors/custom/cytoscape/cytoscape-dagre.js"></script>
</#macro>

<#macro pageContent>
    <@k.subheader pagetitle="Decision trees" level1="Analyse" level2="Tree"></@k.subheader>

    <div class="k-content k-grid__item k-grid__item--fluid" id="k_content">
        <div class="row">
            <div class="col-lg-12">
                <@k.basicportlet title="Run Analysis">
                    <form>
                        <div class="row" style="margin-bottom: 5%">
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        Table
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <select class="form-control" name="tables" id="tables" onchange="setSelectedTable(this)">
                                            <option>Select</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        Features To Include
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <select class="form-control" name="columns" id="columns">
                                            <option>Select</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        Feature Of Interest
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <select class="form-control" name="target" id="target" onchange="createLabelList(this)">
                                            <option>Select</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        Feature Instance
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <select class="form-control"  name="labels" id="labels">
                                            <option>Select</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span>Minimum Leaf Size: <span id="depthSliderVal"></span></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <b style="margin-right: 3%">Shallow</b><input id="depth" type="text", data-slider-min="Shallow", data-slider-max="Deep"><b style="margin-left: 3%">Deep</b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <button type="button" class="btn btn-primary btn-lg" onclick="runTree()">Run Decision Tree</button>
                            </div>
                        </div>
                    </form>
                </@k.basicportlet>
            </div>
        </div>

    <!-- begin:: Content -->
    <#--<div class="k-content k-grid__item k-grid__item--fluid" id="k_content">-->
    <!--begin::Dashboard 1-->
    <#--<div class="row">-->
    <#--<div class="col-lg-4">-->
    <#--<@k.basicportlet title="Select">-->
        <#--<form>-->
            <#--<label>Choose Table</label>-->
            <#--<select class="selectBox" name="tables" id="tables" onchange="setTable(this)">-->
                <#--<option>Select an option</option>-->
            <#--</select>-->
            <#--<br><br>-->
            <#--<label>Choose Columns</label>-->
            <#--<div class="multiselect">-->
                <#--<div class="selectBox" onclick="showCheckboxes()">-->
                    <#--<select>-->
                        <#--<option>Select</option>-->
                    <#--</select>-->
                    <#--<div class="overSelect"></div>-->
                <#--</div>-->
                <#--<div id="checkboxes" style="background-color: #f8f9fa;"></div>-->
            <#--</div>-->
            <#--<br><br>-->
            <#--<button type="button" class="btn btn-primary btn-lg" onclick="runTree()">Run Decision Tree</button>-->
            <#--<label>Choose Column as Label</label>-->
            <#--<select class="selectBox" name="columnnames" id="columnnames" onchange="setColumn(this)">-->
                <#--<option>Select an option</option>-->
            <#--</select>-->
            <#--<br><br>-->
            <#--<button type="button" class="btn btn-primary btn-lg" onclick="viewTree()">View Decision Tree</button>-->
        <#--</form>-->
    <#--</@k.basicportlet>-->

    <#--</div>-->

    <div class="row">
        <div class="col-md-12">
            <@k.heightportlet title="Tree" height="300px">
                <table id="tabledata" class="table table-striped table-bordered"></table>
                <div id="cy"></div>
            </@k.heightportlet>
        </div>
    </div>


</#macro>

<#macro pageScript>
    <!-- Code Editor scripts -->
     <script src="/assets/vendors/custom/slider/bootstrap-slider.js" type="text/javascript"></script>
    <script type="text/javascript" src="/assets/vendors/custom/multiselect/js/bootstrap-multiselect.js"></script>
    <script src="/assets/vendors/custom/jpalette/jpalette.js"></script>
    <script>


        var maxMin = 1000;

        $("#depth").slider({step: 2, min: 0, max: maxMin - 1, value: maxMin - 1});
        $("#depthSliderVal").text(1);
        $("#depth").on("slide", function(slideEvt) {
            $("#depthSliderVal").text(maxMin - slideEvt.value);
        });

        var tables;
        var columns;
        $.get('/api/v1/datacombination/data/datasetInfo', {}, function(data) {
            tables = Object.keys(data);
            columns = data;
        });


        findTable = function(word, namelist){
            if(word == 'table'){
                for(var i = 0; i < tables.length; i++){
                    namelist.push(tables[i])
                }
                return namelist;
            }
            if(tables.includes(word)){
                var colArray = columns[word];
                for(var i = 0; i < colArray.length; i++){
                    namelist.push(word+"."+colArray[i])
                }
                return namelist;
            }
            if(word!='table' & !tables.includes(word)){
                namelist.push(word + ".");
                return namelist;
            }
            return namelist;
        }


        getDefinitions = function(e) {
            var temp = editor.getValue();
            var $this = $(e);
            $this.button('loading');
            if(temp === ""){
                $this.button('reset');
                buildSelect;
            }
            else {
                $.when($.get('/api/v1/datacombination/data/info', {
                    queryinfo: temp
                })).done(function () {
                    $this.button('reset');
                    buildSelect;
                });
            }
        };

    buildClassifierSelect = function(element, options){
        removeOptions(document.getElementById(element));
        var o = new Option('select', 'select');
        $('#'+ element).append(o);
        for(var c in options){
            var o = new Option(options[c], options[c]);
            $(o).html(options[c]);
            $('#'+element).append(o);
        }
    }

        buildSelect = function(element){
            $.get('/api/v1/datacombination/data/availableTables',{},function(options) {
                    removeOptions(document.getElementById(element));
                    // var option = $('<option>');
                    var o = new Option('select', 'select');
                    // option.attr('select', 'select').text('select');
                    $('#'+ element).append(o);
                    for (var val in options) {
                        var o = new Option(options[val], options[val]);
                        $(o).html(options[val]);
                        // option.attr(options[val], options[val]).text(options[val]);
                        $('#'+element).append(o);
                    }
                }
            )
        }

        // removeOptions = function(selectbox)
        // {
        //     var i;
        //     for(i = selectbox.options.length - 1 ; i >= 0 ; i--)
        //     {
        //         selectbox.remove(i);
        //     }
        // }

        removeOptions = function(selectbox)
        {
            for(let i = selectbox.options.length - 1 ; i >= 0 ; i--)
            {
                selectbox.options.remove(i);
            }
        }

        createLabelList = function(selectedObject){
            $.get("/api/v1/datacombination/data/columnvalues",{
                tablename: table, columnname: selectedObject[selectedObject.value].text
            },function(options){
                removeOptions(document.getElementById('labels'));
                let id = 0;
                let o = new Option("Multi-Class", id);
                $(o).html("Multi-Class");
                $('#labels').append(o);
                if(getSelectedText('target') === "All"){
                    let o = new Option("Binary", id);
                    $(o).html("Binary");
                    $('#labels').append(o);
                }
                else{
                    for (let val in options) {
                        let o = new Option(options[val], val);
                        $(o).html(options[val]);
                        $('#labels').append(o);
                    }
                }
            });
        };

        var table;
        var column;
        var allColumns = [];

        setTable = function(selectedObject){
            table = selectedObject.value;
            $.get("/api/v1/datacombination/data/columnnames",{
                table: selectedObject.value
            },function(options){
                removeOptions(document.getElementById('columnnames'));
                // var option = $('<option>');
                var o = new Option('select', 'select');
                // option.attr('select', 'select').text('select');
                $('#columnnames').append(o);
                var checkboxes = document.getElementById("checkboxes");
                var parser = new DOMParser()
                for (var val in options) {
                    var o = new Option(options[val], options[val]);
                    $(o).html(options[val]);
                    // option.attr(options[val], options[val]).text(options[val]);
                    $('#columnnames').append(o);
                    var docfrag = "<label for = " + val.toString() + "><input type=\"checkbox\" id = "+ val.toString()+ " />" + options[val] + "</label>";
                    var doc = document.createRange().createContextualFragment(docfrag);
                    checkboxes.appendChild(doc.firstChild);
                    allColumns.push(options[val]);
                }
            });
        };

        datatable = function(element, columns, data, sorting, columnDefs, scrollX) {
            var dom = "<'row' <'col-md-12'B>><'row  table-scrollable'<'col-md-12 col-sm-12't>>"; // horizobtal scrollable datatable
            // var dom = "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"; // horizobtal scrollable datatable
            return $(element).DataTable({
                dom: dom,
                data: data,
                columns: columns,
                responsive: !scrollX,
                deferRender: true,
                destroy: true,
                lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
                buttons: [
                    {extend: 'print', className: 'btn grey-salt btn-outline'},
                    {extend: 'copy', className: 'btn yellow-haze btn-outline'},
                    {extend: 'excel', className: 'btn green-meadow btn-outline' },
                    {extend: 'csv', className: 'btn blue btn-outline'},
                    /*            {
                     text: 'Reload',
                     className: 'btn yellow btn-outline',
                     action: function (e, dt, node, config) {
                     //dt.ajax.reload();
                     updateTransxTable()
                     }
                     }*/
                ],
                autoWidth: true,
                aaSorting: sorting ? sorting : [[ 0, "desc" ]],
                columnDefs: columnDefs,
                paging : false,
                scrollX: true,
                scrollY: 300,
                fixedColumns:   {
                    heightMatch: 'none'
                }
            });
        };


        buildColumnSelect = function(element){
            $.get('/api/v1/datacombination/data/columnnames',{},function(options) {
                    removeOptions(document.getElementById(element));
                    // var option = $('<option>');
                    var o = new Option('select', 'select');
                    // option.attr('select', 'select').text('select');
                    $('#'+ element).append(o);
                    for (var val in options) {
                        var o = new Option(options[val], options[val]);
                        $(o).html(options[val]);
                        // option.attr(options[val], options[val]).text(options[val]);
                        $('#'+element).append(o);
                    }
                }
            )
        };

        setColumn = function(selectedObject){
            column = selectedObject.value;
        };

        const getNestedObject = (nestedObj, pathArr) => {
            return pathArr.reduce((obj, key) =>
                (obj && obj[key] !== 'undefined') ? obj[key] : undefined, nestedObj);
        };

        var classColours = new Map();
        var colourNum = new Set();
        var colourMap = jPalette.ColorMap.get('rgb')(50);

        selectColour = function(classname){
            let num = Math.floor(Math.random() * 50);
            while(colourNum.has(num)){
                num = Math.floor(Math.random() * 50);
            }
            if(!classColours.has(classname)){
                colourNum.add(num);
                classColours.set(classname,colourMap['map'][num].rgb());
            }
            return classColours.get(classname);
        };

        createTree = function(data, element){
            let children = getNestedObject(data,['children']);
            let edgeLabel = getNestedObject(data,['value','op']) + " " + getNestedObject(data,['value','val']);
            let nodeId = getNestedObject(data,['id']);
            let sourceId = getNestedObject(data,['parent']);

            let shape = getNestedObject(data,['leaf']) === true ? 'rectangle' : 'ellipse';

            let colour = getNestedObject(data,['leaf']) === true ? selectColour(getNestedObject(data,['value','class'])) : '#11479e';

            let leafLabel = getNestedObject(data,['leaf']) === true ? "Class: " + getNestedObject(data,['value','class']) + "\n Number of data points: " +
                getNestedObject(data,['value','count']) + "\n Number of errors: " +
                getNestedObject(data,['value','errors']) : null;
            let splitOn = getNestedObject(data,['children',0,'value','attr']) != null ? getNestedObject(data,['children',0,'value','attr']): leafLabel;

            element.elements.nodes.push({ "data": { "id": 'n' + nodeId.toString(), "nodeType": shape, "nodeColour": colour, "splitOn": splitOn} });
            if(sourceId != null) {
                element.elements.edges.push({"data": {"source": 'n' + sourceId.toString(), "target": 'n' + nodeId.toString(), "label": edgeLabel}});
            }
            for(let child in children) {
                createTree(children[child], element);
            }
        };

        var nodeArray = [];
        var edgeArray= [];


        var initialTree  = {
            container: document.getElementById('cy'),

            boxSelectionEnabled: false,
            autounselectify: true,

            layout: {
                name: 'dagre'
            },

            style: [
                {
                    selector: 'node',
                    style: {
                        'content': 'data(splitOn)',
                        'shape': 'data(nodeType)',
                        'width': 110,
                        'background-opacity': 0.3,
                        'font-size': 9,
                        'text-opacity': 1,
                        'text-valign': 'center',
                        'text-halign': 'center',
                        "text-wrap": "wrap",
                        'background-color': 'data(nodeColour)'
                    }
                },

                {
                    selector: 'edge',
                    style: {
                        'label': 'data(label)',
                        'font-size': 7,
                        'curve-style': 'bezier',
                        'width': 4,
                        'target-arrow-shape': 'triangle',
                        'text-halign': 'left',
                        'line-color': '#9dbaea',
                        'target-arrow-color': '#9dbaea'
                    }
                }
            ],

            elements: {
                nodes: nodeArray,
                edges: edgeArray
            },
        };

        setSelectedTable = function(selectedObject){
            table = selectedObject.value;
            $.get("/api/v1/datacombination/data/columnnames",{
                table: selectedObject.value
            },function(options){
                removeOptions(document.getElementById('columns'));
                removeOptions(document.getElementById('target'));
                removeOptions(document.getElementById('labels'));

                let l = new Option("Select", 0);
                $('#labels').append(l);

                let el = document.getElementById('columns');
                let id = 0;
                let t = new Option("All", id);
                $(t).html("All");
                $('#target').append(t);
                let hml = el.outerHTML;
                if(!hml.includes('multiple')){
                    hml = hml.replace('\"columns\">','\"columns\" multiple = \"multiple\">');
                    document.getElementById('columns').outerHTML = hml;
                }
                for (let val in options) {
                    id++;
                    let o = new Option(options[id - 1], id - 1);
                    let t = new Option(options[id - 1], id);
                    $(o).html(options[val]);
                    $(t).html(options[val]);
                    $('#columns').append(o);
                    $('#target').append(t);
                }
                let o = new Option("Multi-Class", 1);
                $(o).html("Multi-Class");
                $('#labels').append(o);
                if(getSelectedText('target') === "All"){
                    let o = new Option("Binary", 2);
                    $(o).html("Binary");
                    $('#labels').append(o);
                }
                $('#columns').multiselect({includeSelectAllOption: true, maxHeight: 300});
                $('#columns').multiselect('rebuild');

                $.get("/api/v1/datacombination/data/tablerows",{
                    table: selectedObject.value
                },function(rows){
                    maxMin =  Math.round(rows * 0.25);
                    $("#depth").slider({step: 2, min: 0, max: maxMin - 1, value: maxMin - 1});
                });
           });

        };

        getSelectedText = function(elementId) {
            var elt = document.getElementById(elementId);

            if (elt.selectedIndex == -1)
                return "Empty";

            return elt.options[elt.selectedIndex].text;
        };

        getSelectedTextList = function(elementId) {
            var elt = document.getElementById(elementId);

            if (elt.selectedIndex == -1)
                return "Empty";

            var sel = [];
            for(let i in elt.options){
                if(elt.options[i].selected ===true){
                    sel.push(elt.options[i].text);
                }
            }

            return sel;
        };

    runTree = function(){
        var selectedColumns = [];
        for(var i = 0; i < allColumns.length; i++){
            if(document.getElementById(i.toString()).checked === true){
                selectedColumns.push(allColumns[i]);
            }
        }
        let table = getSelectedText('tables').toString();
        let selectedFeatures = getSelectedTextList('columns').toString();
        let target = getSelectedText('target').toString();
        let instance = getSelectedText('labels').toString();
        $.get('/api/v1/datacombination/data/decisiontree',{
            table: table,
            selectedFeatures: selectedFeatures,
            target: target,
            instance: instance,
            depth: 10
        },function(data){
            // buildClassifierSelect('columnnames',data);
            viewTree(target, instance)
        })
    };

    viewTree = function(target, instance){
        $.get('/api/v1/datacombination/data/decisiontree/jsontree',{
            column: target,
            target: instance
        },function(data){
            initialTree.elements.nodes = [];
            initialTree.elements.edges = [];
            classColours = new Map();
            colourNum = new Set();
            createTree(data, initialTree);
            var cy = window.cy = cytoscape(initialTree);
        })
    };

        // [
        //     { data: { id: 'n0', nodeType: 'rectangle', nodeColour: '#11479e' } },
        //     { data: { id: 'n1', nodeType: 'rectangle', nodeColour: '#11479e' } },
        //     { data: { id: 'n2', nodeType: 'ellipse', nodeColour: '#B8E48C' } },
        //     { data: { id: 'n3', nodeType: 'rectangle', nodeColour: '#11479e' } },
        //     { data: { id: 'n4', nodeType: 'ellipse', nodeColour: '#B8E48C' } },
        //     { data: { id: 'n5', nodeType: 'ellipse', nodeColour: '#B8E48C' } }
        // ]
        // [
        //     { data: { source: 'n0', target: 'n1' } },
        //     { data: { source: 'n1', target: 'n2' } },
        //     { data: { source: 'n1', target: 'n3' } },
        //     { data: { source: 'n3', target: 'n4' } },
        //     { data: { source: 'n3', target: 'n5' } },
        // ]

        var expanded = false;

        function showCheckboxes() {
            var checkboxes = document.getElementById("checkboxes");
            if (!expanded) {
                checkboxes.style.display = "block";
                expanded = true;
            } else {
                checkboxes.style.display = "none";
                expanded = false;
            }
        }

        buildSelect('tables');


    </script>

</#macro>