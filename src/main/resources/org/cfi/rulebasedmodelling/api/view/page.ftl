<#-- @ftlvariable name="" type="org.cfi.rulebasedmodelling.api.view.PageView" -->
<#import "${contentFtl}" as content>

<!DOCTYPE html>

<!--
Theme: Keen - The Ultimate Bootstrap Admin Theme
Website: http://www.keenthemes.com/
License: You must have a valid license purchased only from https://themes.getbootstrap.com/product/keen-the-ultimate-bootstrap-admin-theme/ in order to legally use the theme for your project.
-->
<html lang="en" >
<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>
    <title><@content.pageTitle /></title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!--begin::Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({

            google: {
                "families":[
                    "Poppins:300,400,500,600,700"]},

            active: function() {

                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Fonts -->
    <!--begin::Page Vendors Styles(used by this page) -->
    <!--end::Page Vendors Styles -->
    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <!--RTL version:<link href="/assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
    <link href="/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--RTL version:<link href="/assets/demo/default/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
    <!--end::Global Theme Styles -->
    <!--begin::Layout Skins(used by all pages) -->
    <link href="/assets/demo/default/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <!--RTL version:<link href="/assets/demo/default/skins/header/base/light.rtl.css" rel="stylesheet" type="text/css" />-->
    <link href="/assets/demo/default/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <!--RTL version:<link href="/assets/demo/default/skins/header/menu/light.rtl.css" rel="stylesheet" type="text/css" />-->
    <link href="/assets/demo/default/skins/brand/navy.css" rel="stylesheet" type="text/css" />
    <!--RTL version:<link href="/assets/demo/default/skins/brand/navy.rtl.css" rel="stylesheet" type="text/css" />-->
    <link href="/assets/demo/default/skins/aside/navy.css" rel="stylesheet" type="text/css" />
    <!--RTL version:<link href="/assets/demo/default/skins/aside/navy.rtl.css" rel="stylesheet" type="text/css" />-->
    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="/assets/media/logos/favicon.ico" />

    <style>
        .k-aside--minimize .k-aside__brand-logo {
            display: none;
        }

        .truncate {
            /*width: 250px;*/
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
    </style>

    <!-- INCLUDE TEMPLATE PAGE HEAD -->
    <@content.pageHead />
</head>
<!-- end::Head -->
<!-- begin::Body -->
<body class="k-header--fixed k-header-mobile--fixed k-subheader--enabled k-subheader--transparent k-aside--enabled k-aside--fixed k-page--loading" >
<!-- begin:: Header Mobile -->
<div id="k_header_mobile" class="k-header-mobile k-header-mobile--fixed " >
    <div class="k-header-mobile__logo">
        <a href="index.html">
            R B M
        </a>
    </div>
    <div class="k-header-mobile__toolbar">
        <button class="k-header-mobile__toolbar-toggler k-header-mobile__toolbar-toggler--left" id="k_aside_mobile_toggler"><span></span></button>
        <button class="k-header-mobile__toolbar-toggler" id="k_header_mobile_toggler"><span></span></button>
        <button class="k-header-mobile__toolbar-topbar-toggler" id="k_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
    </div>
</div>
<!-- end:: Header Mobile -->
<!-- begin:: Root -->
<div class="k-grid k-grid--hor k-grid--root">
    <!-- begin:: Page -->
    <div class="k-grid__item k-grid__item--fluid k-grid k-grid--ver k-page">
        <!-- begin:: Aside -->
        <button class="k-aside-close " id="k_aside_close_btn"><i class="la la-close"></i></button>
        <div class="k-aside k-aside--fixed k-grid__item k-grid k-grid--desktop k-grid--hor-desktop" id="k_aside">
            <!-- begin::Aside Brand -->
            <div class="k-aside__brand k-grid__item " id="k_aside_brand">
                <div class="k-aside__brand-logo">
                    <a href="/api/v1/ui" style="font-size: 24px;">
                        R B M
                    </a>
                </div>
                <div class="k-aside__brand-tools">
                    <button class="k-aside__brand-aside-toggler k-aside__brand-aside-toggler--left" id="k_aside_toggler"><span></span></button>
                </div>
            </div>
            <!-- end:: Aside Brand -->
            <!-- begin:: Aside Menu -->
            <div class="k-aside-menu-wrapper k-grid__item k-grid__item--fluid" id="k_aside_menu_wrapper">
                <div id="k_aside_menu" class="k-aside-menu " data-kmenu-vertical="1" data-kmenu-scroll="1" data-kmenu-dropdown-timeout="500" >
                    <ul class="k-menu__nav ">

                        <li class="k-menu__item " aria-haspopup="true" >
                            <a href="/api/v1/ui/dashboard" class="k-menu__link ">
                                <i class="k-menu__link-icon flaticon2-architecture-and-city"></i><span class="k-menu__link-text">Home</span>
                            </a>
                        </li>
                        <li class="k-menu__section ">
                            <h4 class="k-menu__section-text">Prepare</h4>
                            <i class="k-menu__section-icon flaticon-more-v2"></i>
                        </li>
                        <li class="k-menu__item " >
                            <a href="/api/v1/ui/loaddata" class="k-menu__link ">
                                <i class="k-menu__link-icon la la-plus-circle"></i><span class="k-menu__link-text">Import</span>
                            </a
                        </li>
                        <#--<li class="k-menu__item " >-->
                            <#--<a href="/api/v1/ui/combinationRules" class="k-menu__link ">-->
                                <#--<i class="k-menu__link-icon la la-code"></i><span class="k-menu__link-text">Recombine</span>-->
                            <#--</a-->
                        <#--</li>-->
                        <#--<li class="k-menu__item " >-->
                            <#--<a href="/api/v1/ui/datacombine" class="k-menu__link ">-->
                                <#--<i class="k-menu__link-icon la la-puzzle-piece"></i><span class="k-menu__link-text">Recombine</span>-->
                            <#--</a-->
                        <#--</li>-->

                        <li class="k-menu__section ">
                            <h4 class="k-menu__section-text">Analyse</h4>
                            <i class="k-menu__section-icon flaticon-more-v2"></i>
                        </li>

                        <#--<li class="k-menu__item " >-->
                            <#--<a href="/api/v1/ui/visualise" class="k-menu__link ">-->
                                <#--<i class="k-menu__link-icon la la-eye"></i><span class="k-menu__link-text">Overview</span>-->
                            <#--</a-->
                        <#--</li>-->
                        <li class="k-menu__item " >
                            <a href="/api/v1/ui/viewData" class="k-menu__link ">
                                <i class="k-menu__link-icon la la-table"></i><span class="k-menu__link-text">Tables</span>
                            </a
                        </li>
                        <li class="k-menu__item " >
                            <a href="/api/v1/ui/ModelResults" class="k-menu__link ">
                                <i class="k-menu__link-icon la la-sitemap"></i><span class="k-menu__link-text">Create Model</span>
                            </a
                        </li>
                        <#--<li class="k-menu__item " >-->
                            <#--<a href="/api/v1/ui/decisiontree" class="k-menu__link ">-->
                                <#--<i class="k-menu__link-icon la la-sitemap"></i><span class="k-menu__link-text">Tree</span>-->
                            <#--</a-->
                        <#--</li>-->

                        <li class="k-menu__section ">
                            <h4 class="k-menu__section-text">Run</h4>
                            <i class="k-menu__section-icon flaticon-more-v2"></i>
                        </li>

                        <li class="k-menu__item " >
                            <a href="/api/v1/ui/ModelOutput" class="k-menu__link ">
                                <i class="k-menu__link-icon la la-sitemap"></i><span class="k-menu__link-text">Run Model</span>
                            </a
                        </li>

                        <li class="k-menu__section ">
                            <h4 class="k-menu__section-text">System</h4>
                            <i class="k-menu__section-icon flaticon-more-v2"></i>
                        </li>

                    </ul>
                </div>
            </div>
            <!-- end:: Aside Menu -->
            <!-- begin:: Aside Footer -->
            <div class="k-aside__footer k-grid__item" id="k_aside_footer">
                <div class="k-aside__footer-nav">
                    <div class="k-aside__footer-item">
                        <a href="#" class="btn btn-icon"><i class="flaticon2-gear"></i></a>
                    </div>
                    <div class="k-aside__footer-item">
                        <a href="#" class="btn btn-icon"><i class="flaticon2-cube"></i></a>
                    </div>
                </div>
            </div>
            <!-- end:: Aside Footer-->
        </div>
        <!-- end:: Aside -->
        <!-- begin:: Wrapper -->
        <div class="k-grid__item k-grid__item--fluid k-grid k-grid--hor k-wrapper" id="k_wrapper">
            <!-- begin:: Header -->
            <div id="k_header" class="k-header k-grid__item k-header--fixed " >
                <!-- begin:: Header Menu -->
                <div class="k-header-menu-wrapper" id="k_header_menu_wrapper">
                    <div id="k_header_menu" class="k-header-menu k-header-menu-mobile  k-header-menu--layout- ">
                    </div>
                </div>
                <!-- end:: Header Menu -->
                <!-- begin:: Header Topbar -->
                <div class="k-header__topbar">
                    <!--begin: Quick Actions -->
                    <div class="k-header__topbar-item">
                        <div class="k-header__topbar-wrapper" id="k_offcanvas_toolbar_quick_actions_toggler_btn">
                            <span class="k-header__topbar-icon"><i class="flaticon2-gear"></i></span>
                        </div>
                    </div>
                    <!--end: Quick Actions -->
                </div>
                <!-- end:: Header Topbar -->
            </div>
            <!-- end:: Header -->
            <div class="k-grid__item k-grid__item--fluid k-grid k-grid--hor">

                <@content.pageContent />


                <!-- end:: Subheader -->

            </div>
            <!-- begin:: Footer -->
            <div class="k-footer k-grid__item k-grid k-grid--desktop k-grid--ver-desktop">
                <div class="k-footer__copyright"> 2018&nbsp;&copy;&nbsp;Centre for Innovation, Leiden University</div>
                <div class="k-footer__menu"> <a href="" class="k-footer__menu-link k-link">About</a> </div>
            </div>
            <!-- end:: Footer -->
        </div>
        <!-- end:: Wrapper -->
    </div>
    <!-- end:: Page -->
</div>
<!-- end:: Root -->
<!-- begin:: Scrolltop -->
<div id="k_scrolltop" class="k-scrolltop"> <i class="la la-arrow-up"></i> </div>
<!-- end:: Scrolltop -->
<iframe id="download_iframe" style="display:none;"></iframe>

<script>
    var KAppOptions = {

        "colors": {
            "state": {
                "brand": "#5d78ff",
                "metal": "#c4c5d6",
                "light": "#ffffff",
                "accent": "#00c5dc",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995",
                "focus": "#9816f4"
            },

            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],

                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
</script>
<!-- end::Global Config -->
<!--begin::Global Theme Bundle(used by all pages) -->
<script src="/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
<script src="/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
<script src="/assets/vendors/custom/cfi/cfi.util.js" type="text/javascript"></script>
<script src="/assets/vendors/custom/cfi/cfi.clientstate.js" type="text/javascript"></script>
<script src="/assets/vendors/custom/cfi/cfi.ui.js" type="text/javascript"></script>
<script src="/assets/vendors/custom/cfi/cfi.dbdoc.js" type="text/javascript"></script>
<!--end::Global Theme Bundle -->
<!--begin::Page Vendors(used by this page) -->
<!--end::Page Vendors -->
<!--begin::Page Scripts(used by this page) -->
<#--<script src="/assets/app/scripts/custom/dashboard.js" type="text/javascript"></script>-->
<!--end::Page Scripts -->
<!--begin::Global App Bundle(used by all pages) -->
<#--<script src="/assets/app/scripts/bundle/app.bundle.js" type="text/javascript"></script>-->
<!--end::Global App Bundle -->

<script>
    $(document).ready(() => {
        var daterangepickerInit = function() {
            if ($('#k_dashboard_daterangepicker').length == 0) {
                return;
            }

            var picker = $('#k_dashboard_daterangepicker');
            var start = moment();
            var end = moment();

            function cb(start, end, label) {
                var title = '';
                var range = '';

                if ((end - start) < 100 || label == 'Today') {
                    title = 'Today:';
                    range = start.format('MMM D');
                } else if (label == 'Yesterday') {
                    title = 'Yesterday:';
                    range = start.format('MMM D');
                } else {
                    range = start.format('MMM D') + ' - ' + end.format('MMM D');
                }

                picker.find('#k_dashboard_daterangepicker_date').html(range);
                picker.find('#k_dashboard_daterangepicker_title').html(title);
            }

            picker.daterangepicker({
                direction: KUtil.isRTL(),
                startDate: start,
                endDate: end,
                opens: 'left',
                applyClass: "btn btn-sm btn-primary",
                cancelClass: "btn btn-sm btn-secondary",
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end, '');
        }
        daterangepickerInit();
    })
</script>

<@content.pageScript />

</body>
<!-- end::Body -->
</html>