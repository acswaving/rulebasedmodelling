<#import "keen.ftl" as k>

<#macro pageTitle>
    Intelligent Risk Monitoring - Dashboard
</#macro>

<#macro pageHead></#macro>

<#macro pageContent>
    <@k.subheader pagetitle="Dashboard" level1="Home" level2="Dashboard"></@k.subheader>


    <!-- begin:: Content -->
    <div class="k-content k-grid__item k-grid__item--fluid" id="k_content">
        <!--begin::Dashboard 1-->
        <div class="row">
            <div class="col-lg-12">
                <h1 style="color: #959cb6;">Rule Based Modelling</h1>
                <p>Description</p>
            </div>
        </div>

        <!--begin::Row-->
        <div class="row">
            <div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">
                <!--begin::Portlet-->
                <div class="k-portlet k-portlet--fit k-portlet--height-fluid">
                    <div class="k-portlet__body k-portlet__body--fluid">
                        <div class="k-widget-3 k-widget-3--brand">
                            <div class="k-widget-3__content">
                                <div class="k-widget-3__content-info">
                                    <div class="k-widget-3__content-section">
                                        <div class="k-widget-3__content-title">Data Management</div>
                                        <div class="k-widget-3__content-desc">Recombine existing data</div>
                                        <a href="combinationRules" class="text-light"> <i class="la la-2x la-arrow-circle-o-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->	</div>
            <div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">
                <!--begin::Portlet-->
                <div class="k-portlet k-portlet--fit k-portlet--height-fluid">
                    <div class="k-portlet__body k-portlet__body--fluid">
                        <div class="k-widget-3 k-widget-3--danger">
                            <div class="k-widget-3__content">
                                <div class="k-widget-3__content-info">
                                    <div class="k-widget-3__content-section">
                                        <div class="k-widget-3__content-title">Data Exploration</div>
                                        <div class="k-widget-3__content-desc">Reveal underlying patterns</div>
                                        <a href="ModelResults" class="text-light"> <i class="la la-2x la-arrow-circle-o-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->	</div>
            <div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">
                <!--begin::Portlet-->
                <div class="k-portlet k-portlet--fit k-portlet--height-fluid">
                    <div class="k-portlet__body k-portlet__body--fluid">
                        <div class="k-widget-3 k-widget-3--success">
                            <div class="k-widget-3__content">
                                <div class="k-widget-3__content-info">
                                    <div class="k-widget-3__content-section">
                                        <div class="k-widget-3__content-title">Data Visualisation</div>
                                        <div class="k-widget-3__content-desc">Communicate insights</div>
                                        <a href="visualise" class="text-light"> <i class="la la-2x la-arrow-circle-o-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->	</div>
        </div>
        <!--end::Row-->

        <!--end::Dashboard 1-->
    </div>
    <!-- end:: Content -->

</#macro>

<#macro pageScript>



</#macro>