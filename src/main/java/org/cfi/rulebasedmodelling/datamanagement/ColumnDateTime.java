package org.cfi.rulebasedmodelling.datamanagement;

import org.cfi.rulebasedmodelling.time.DateTimeUtil;
import org.joda.time.DateTime;

public class ColumnDateTime extends ColumnData{

    private int count = 0;
    private int missing = 0;
    private DateTime earliest = new DateTime(4500,1,01,0,0);
    private DateTime latest = new DateTime(0,1,1,0,0);

    public ColumnDateTime(String tableName, int order){
        super(tableName, order);
    }

    @Override
    public void update(String value){
        try{
            DateTime dt = DateTimeUtil.parseLiberalDateTime(value);
            updateCount();
            updateEarliest(dt);
            updateLatest(dt);
        }
        catch(Exception e){
            updateCount();
            this.missing++;
        }
    }

    void updateCount(){
        this.count++;
    }

    void updateEarliest(DateTime dt){
        earliest = dt.isBefore(earliest) ? dt : earliest;
    }

    void updateLatest(DateTime dt){
        latest = dt.isAfter(latest) ? dt : latest;
    }

    public int getCount() {
        return count;
    }

    public int getMissing() {
        return missing;
    }

    public DateTime getEarliest() {
        return earliest;
    }

    public DateTime getLatest() {
        return latest;
    }

    @Override
    public String toString(){
        return "{\"Earliest\": "+ getEarliest() + ",\"Latest\": " + getLatest() + ",\"Number of Rows\": " + getCount()+ ",\"Missing\":"+ getMissing()+ "}";
    }
}
