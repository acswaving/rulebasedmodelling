package org.cfi.rulebasedmodelling.datamanagement.Query;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.Iterator;

public class DataClassifierDeserializer extends JsonDeserializer<DataClassifier> {

    @Override
    public DataClassifier deserialize(JsonParser jp, DeserializationContext cxt) throws IOException, JsonProcessingException {
        ObjectCodec objectCodec = jp.getCodec();
        ObjectNode arrayNode = objectCodec.readTree(jp);

        String tableName = arrayNode.get("tableName").asText();
        String tableToFilter = arrayNode.get("tableToFilter").asText();
        Iterator<JsonNode> paths = arrayNode.path("pathlist").elements();
        DataClassifier dataClassifier = new DataClassifier();
        while(paths.hasNext()){
            PathFilter pathFilter = new PathFilter();
            Iterator<JsonNode> path = paths.next().elements();
            while(path.hasNext()){
                Filter filter = new Filter();
                JsonNode node = path.next();
                filter.setColumn(node.get("column").asText());
                filter.setFilter(node.get("filter").asText());
                filter.setValue(formatValue(node.get("value")));
                pathFilter.setPathFilters(filter);
            }
          dataClassifier.setTableName(tableName);
          dataClassifier.setTableToFilter(tableToFilter);
          dataClassifier.setPathList(pathFilter);
        }
        return dataClassifier;
    }

    private String formatValue(JsonNode value){
        StringBuilder sb = new StringBuilder();
        if(value.getNodeType().toString().equals("ARRAY")){
            Iterator<JsonNode> jn = value.elements();
            while(jn.hasNext()) {
                JsonNode val = jn.next();
                sb.append("\'" + val.asText() + "\'");
                sb.append(" AND ");
            }
            sb.delete(sb.length()-5, sb.length()-1);
            return sb.toString();
        }
        return value.asText();
    }
}
