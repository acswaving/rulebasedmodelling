package org.cfi.rulebasedmodelling.datamanagement.Query;

public class Filter {

    private String column;
    private String filter;
    private String value;

    public Filter(String column, String filter, String value){
        this.column = column;
        this.filter = filter;
        this.value = value;
    }

    public Filter(){}

    public String getColumn() {
        return column;
    }

    public String getFilter() {
        return filter;
    }

    public String getValue() {
        return value;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString(){
        return this.column + " " + this.filter + " " + this.value;
    }
}
