package org.cfi.rulebasedmodelling.datamanagement.Query;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class DataClassifierTest {

    public static void main(String[] args){
        ObjectMapper objectMapper = new ObjectMapper();

        try{
            String json = "{\"pathlist\":[[{\"column\":\"rain\",\"filter\":\">\",\"value\":\"sun\"}," +
                    "{\"column\":\"sun\",\"filter\":\"<\",\"value\":\"wind\"}],[{\"column\":\"blah\",\"filter\":\">\",\"value\":\"blahblah\"}," +
                    "{\"column\":\"moon\",\"filter\":\"<\",\"value\":\"stars\"}]]}";
            DataClassifier dataClassifier = objectMapper.readValue(json, DataClassifier.class);
            System.out.println(dataClassifier.toString());
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }
}
