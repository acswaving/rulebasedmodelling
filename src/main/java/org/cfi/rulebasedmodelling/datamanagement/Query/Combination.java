package org.cfi.rulebasedmodelling.datamanagement.Query;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.collect.Lists;

import java.util.List;

@JsonDeserialize(using = CombinationDeserializer.class)
public class Combination implements QueryObject {

    private List<CombinationItem> tablesToCombine = Lists.newLinkedList();
//    private String by;
    private List<String> by = Lists.newArrayList();
    private String tableName;

    public Combination(){}

    public List<CombinationItem> getTablesToCombine() { return tablesToCombine; }

    public void setTablesToCombine(CombinationItem tablesToCombine) {
        this.tablesToCombine.add(tablesToCombine);
    }

//    public String getBy() { return by; }

    public List<String> getBy() { return by; }

//    public void setBy(String by) {
//        this.by = by;
//    }
    public void setBy(String by) {
        this.by.add(by);
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

}
