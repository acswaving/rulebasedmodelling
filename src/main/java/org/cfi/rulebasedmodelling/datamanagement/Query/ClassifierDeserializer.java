package org.cfi.rulebasedmodelling.datamanagement.Query;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.Iterator;

public class ClassifierDeserializer extends JsonDeserializer<RuleClassifier> {

    @Override
    public RuleClassifier deserialize(JsonParser jp, DeserializationContext cxt) throws IOException, JsonProcessingException {
        ObjectCodec objectCodec = jp.getCodec();
        ObjectNode arrayNode = objectCodec.readTree(jp);

        RuleClassifier ruleClassifier = new RuleClassifier();

        String ruleTable = arrayNode.get("table").asText();
        String columnLabel = arrayNode.get("columnName").asText();
        String columntype = arrayNode.get("datatype").asText();

        if(arrayNode.get("conditionals") != null) {
            Iterator<JsonNode> conditionals = arrayNode.get("conditionals").elements();
            while (conditionals.hasNext()) {
                JsonNode derivedData = conditionals.next();
                Iterator<String> names = derivedData.fieldNames();
                String derivedName = "";
                while (names.hasNext()) {
                    derivedName = names.next();
                }
                Iterator<JsonNode> paths = derivedData.get(derivedName).elements();
                DataClassifier dataClassifier = new DataClassifier();
                while (paths.hasNext()) {
                    PathFilter pathFilter = new PathFilter();
                    Iterator<JsonNode> path = paths.next().elements();
                    while (path.hasNext()) {
                        Filter filter = new Filter();
                        JsonNode node = path.next();
                        filter.setColumn(node.get("column").asText());
                        filter.setFilter(node.get("filter").asText());
                        filter.setValue(formatValue(node.get("value")));
                        pathFilter.setPathFilters(filter);
                    }
                    dataClassifier.setPathList(pathFilter);
                }
                ruleClassifier.setTable(ruleTable);
                ruleClassifier.setClassifier(columnLabel);
                ruleClassifier.setType(columntype);
                ruleClassifier.setConditionals(derivedName, dataClassifier);
            }
        }
        else{
            ruleClassifier.setTable(ruleTable);
            ruleClassifier.setClassifier(columnLabel);
            ruleClassifier.setType(columntype);
            JsonNode operators = arrayNode.get("operators");
            String calc = unrollOperators(operators, new StringBuilder());
            ruleClassifier.setConditionals(calc, null);
        }

        return ruleClassifier;
    }

    private String unrollOperators(JsonNode jsonNode, StringBuilder sb){
        if(jsonNode.get("value").get("operator") == null){
            sb.append(jsonNode.get("column").asText() + " ");
            sb.append(jsonNode.get("operator").asText() + " ");
            sb.append(jsonNode.get("value").asText() + " ");
        }
        else{
            sb.append(jsonNode.get("column").asText() + " ");
            sb.append(jsonNode.get("operator").asText() + " (");
            unrollOperators(jsonNode.get("value"), sb);
            sb.append(")");
        }
        return sb.toString();
    }



    private String formatValue(JsonNode value){
        StringBuilder sb = new StringBuilder();
        if(value.getNodeType().toString().equals("ARRAY")){
            Iterator<JsonNode> jn = value.elements();
            while(jn.hasNext()) {
                JsonNode val = jn.next();
                sb.append("\'" + val.asText() + "\'");
                sb.append(" AND ");
            }
            sb.delete(sb.length()-5, sb.length()-1);
            return sb.toString();
        }
        return value.asText();
    }
}
