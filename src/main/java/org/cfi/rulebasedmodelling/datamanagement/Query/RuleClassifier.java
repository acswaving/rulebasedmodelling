package org.cfi.rulebasedmodelling.datamanagement.Query;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.collect.Maps;

import java.util.Map;

@JsonDeserialize(using = ClassifierDeserializer.class)
public class RuleClassifier implements QueryObject {

    private String table;
    private String classifier;
    private String type;
    private Map<String,DataClassifier> conditionals = Maps.newLinkedHashMap();

    public RuleClassifier(){}

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getClassifier() {
        return classifier;
    }

    public void setClassifier(String classifier) {
        this.classifier = classifier;
    }

    public Map<String, DataClassifier> getConditionals() {
        return conditionals;
    }

    public void setConditionals(String className,DataClassifier conditional) {
        this.conditionals.put(className,conditional);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
