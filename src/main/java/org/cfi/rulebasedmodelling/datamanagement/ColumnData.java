package org.cfi.rulebasedmodelling.datamanagement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class ColumnData implements Comparable<ColumnData> {

    @JsonIgnore
    protected int order;
    @JsonProperty
    protected String columnName;
    @JsonProperty
    protected String tableName;

    public ColumnData(String tableName, int order){
        this.tableName = tableName;
        this.order = order;
    }

    public abstract void update(String value);

    @Override
    public abstract String toString();

    public int setColumnName(String columnName){
        this.columnName = columnName;
        return order;
    }

    public int getOrder(){
        return order;
    }

    public int compareTo(ColumnData columnData) {
        return this.order < columnData.order ? -1 : this.order == columnData.order ? 0 : 1;
    }

    public String getColumnName(){
        return this.columnName;
    }
}
