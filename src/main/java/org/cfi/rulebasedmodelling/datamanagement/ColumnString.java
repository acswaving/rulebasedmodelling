package org.cfi.rulebasedmodelling.datamanagement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.assertj.core.util.Sets;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class ColumnString extends ColumnData{

    private int enumLimit;

    private int count = 0;
    private int missing = 0;
    private Map<String,Integer> uniqueValues = Maps.newHashMap();
    private boolean isText = false;

    public ColumnString(String tableName, int order, int enumLimit){
        super(tableName, order);
        this.enumLimit = enumLimit;
    }

    @Override
    public void update(String value){
        if(value==null){
            updateCount();
            this.missing++;
        }
        else {
            updateCount();
            updateUniqueVales(value);
        }
    }

    void updateCount(){
        this.count++;
    }

    void updateUniqueVales(String value){
        if(this.uniqueValues.containsKey(value) && this.uniqueValues.keySet().size() < this.enumLimit){
            this.uniqueValues.put(value,this.uniqueValues.get(value) + 1);
        }
        else if(this.uniqueValues.keySet().size() < this.enumLimit){
            this.uniqueValues.put(value,1);
        }
        else{
            isText = true;
        }
    }

    public int getCount() {
        return count;
    }

    public int getMissing() {
        return missing;
    }

    public Set<String> getUniqueValues() {
        return !isText ? uniqueValues.keySet() : Sets.newHashSet();
    }

    @JsonIgnore
    public List<String> getUniqueValuesJ() {
        List<String> valuesJson = Lists.newArrayList();
        for(String v : getUniqueValues()){
            valuesJson.add("\"" + v + "\"");
        }
        return valuesJson;
    }

    public Map<String,Integer> getValuesAndFrequencies(){
        return !isText ? this.uniqueValues : Maps.newHashMap();
    }

    @JsonIgnore
    public String getValuesAndFrequenciesJ() {
        StringBuilder sb = new StringBuilder("{");
        for(Map.Entry v : getValuesAndFrequencies().entrySet()){
            sb.append("\"" + v.getKey() + "\":" + v.getValue() + ",");
        }
        sb.deleteCharAt(sb.length()-1);
        sb.append("}");
        return !isText ? sb.toString() : "\"too many values for modelling\"";
    }

    public boolean isText() {
        return isText;
    }

    @Override
    public String toString(){
        return "{\"Unique\": "+ getUniqueValuesJ() + ",\"isText\": " + isText() + ",\"Number of Rows\": " + getCount()+ ",\"Missing\":"+ getMissing() + ",\"Values and Frequencies\": "+ getValuesAndFrequenciesJ() +"}";
    }
}
