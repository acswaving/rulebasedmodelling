package org.cfi.rulebasedmodelling.datamanagement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ColumnNumeric extends ColumnData {

    @JsonProperty
    private double sum = 0.0;
    @JsonProperty
    private int count = 0;
    @JsonProperty
    private double max = -1000000000.0;
    @JsonProperty
    private double min = 1000000000.0;
    @JsonProperty
    private int missing = 0;

    public ColumnNumeric(String tableName, int order){
        super(tableName, order);
    }

    @Override
    public void update(String value){
        try {
            double val = Double.parseDouble(value);
            updateCount();
            updateMax(val);
            updateMin(val);
            updateTotal(val);
        }
        catch(Exception e){
            updateCount();
            this.missing++;
        }
    }

    void updateMax(double value){
        this.max = value > this.max ? value : this.max;
    }

    void updateMin(double value){
        this.min = value < this.min ? value : this.min;
    }

    void updateTotal(double value){
        this.sum += value;
    }

    void updateCount(){
        this.count++;
    }

    public double getMean() {
        return sum / (count - missing);
    }

    public int getCount() {
        return count;
    }

    public double getMax() {
        return max;
    }

    public double getMin() {
        return min;
    }

    public int getMissing() {
        return missing;
    }

    @Override
    public String toString(){
        return "{\"Average\": "+ getMean() + ",\"Min\": " + getMin() + ",\"Max\": " + getMax() + ",\"Number_Of_Rows\": " + getCount()+ ",\"Missing\":" + getMissing() + "}";
    }
}
