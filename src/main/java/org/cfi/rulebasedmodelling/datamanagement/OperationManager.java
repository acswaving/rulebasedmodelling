package org.cfi.rulebasedmodelling.datamanagement;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.cfi.rulebasedmodelling.api.Config;
import org.cfi.rulebasedmodelling.datamanagement.Query.Combination;
import org.cfi.rulebasedmodelling.datamanagement.Query.DataClassifier;
import org.cfi.rulebasedmodelling.datamanagement.Query.RuleClassifier;
import org.cfi.rulebasedmodelling.postgres.DataRead;
import org.cfi.rulebasedmodelling.postgres.datamanagement.TableBuilder;
import org.cfi.rulebasedmodelling.postgres.datamanagement.TableCombination;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class OperationManager {

    private String json;
    private static ObjectMapper objectMapper;
    private static Config config;

    public OperationManager(Config config, ObjectMapper objectMapper){
        this.config = config;
        this.objectMapper = objectMapper;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public List<Map<String,TableBuilder>> operationOrder() throws IOException{
        List<Map<String,TableBuilder>> queryObjects = Lists.newArrayList();
        JsonNode fullJson = objectMapper.reader().readTree(this.json);
        Iterator<String> allOperators = fullJson.fieldNames();
        while(allOperators.hasNext()){
            String functionName = allOperators.next();
            String jsonOfInterest = fullJson.get(functionName).toString();
            queryObjects.add(FunctionType.valueOf(functionName.toUpperCase().substring(0,functionName.length()-2)).createTableBuilder(jsonOfInterest));
        }
        return queryObjects;
    }

    public enum FunctionType {
        COMBINE{
            @Override
            public Map<String,TableBuilder> createTableBuilder(String jsonOfInterest) throws IOException {
                Map<String,TableBuilder> table = Maps.newHashMap();
                TableCombination tableCombination = new TableCombination();
                Combination combination = objectMapper.readValue(jsonOfInterest, Combination.class);
                combination.getTablesToCombine().stream()
                        .forEach(ci -> {
                                    tableCombination.addTable(new DataRead(config)
                                                    .buildTable(ci.getTable(),ci.getColumns())
                                                    .aggregateBy(ci),
                                            ci.getJoins(),ci.getColumns(),ci.getTable());
                                }
                        );
                tableCombination.createCombination(combination.getTableName(),combination.getBy());
                table.put(combination.getTableName(),tableCombination.getTableBuilder());
                return table;
            }
        },
        FILTER{
            @Override
            public Map<String,TableBuilder> createTableBuilder(String jsonOfInterest) throws IOException{
                Map<String,TableBuilder> table = Maps.newHashMap();
                DataClassifier dataClassifier = objectMapper.readValue(jsonOfInterest, DataClassifier.class);
                TableBuilder tableBuilder = new DataRead(config).buildTable(dataClassifier.getTableToFilter(),new ArrayList<>());
                table.put(dataClassifier.getTableName(),tableBuilder.conditional(dataClassifier.toString(),dataClassifier.getTableToFilter()));
                return table;
            }
        },
        DERIVED_DATA{
            @Override
            public Map<String,TableBuilder> createTableBuilder(String jsonOfInterest) throws IOException{
                Map<String,TableBuilder> table = Maps.newHashMap();
                RuleClassifier ruleClassifier = objectMapper.readValue(jsonOfInterest, RuleClassifier.class);
                TableBuilder tableBuilder = new TableBuilder();
                tableBuilder.derivedDataFilters(ruleClassifier);
                table.put(ruleClassifier.getTable(),tableBuilder);
                return table;
            }
        };

        public abstract Map<String,TableBuilder> createTableBuilder(String jsonOfInterest) throws IOException;

    }


}
