package org.cfi.rulebasedmodelling.api.resourses;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.csv.CSVFormat;
import org.cfi.rulebasedmodelling.api.Config;
import org.cfi.rulebasedmodelling.datamanagement.OperationManager;
import org.cfi.rulebasedmodelling.datamanagement.summaries.DataSummary;
import org.cfi.rulebasedmodelling.ml.*;
import org.cfi.rulebasedmodelling.postgres.DataImport;
import org.cfi.rulebasedmodelling.postgres.DataRead;
import org.cfi.rulebasedmodelling.postgres.datamanagement.ColumnDefinitions;
import org.cfi.rulebasedmodelling.postgres.datamanagement.DataSet;
import org.cfi.rulebasedmodelling.postgres.datamanagement.TableBuilder;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import weka.core.Attribute;
import weka.core.Instances;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.List;
import java.util.Map;

@Path("/datacombination")
@Api("/datacombination")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class DataCombinationResource {

    private final Logger logger = LoggerFactory.getLogger(DataCombinationResource.class);

    private Config config;
    private ObjectMapper objectMapper;
    private OperationManager operationManager;
    private Map<String, Map<String, Label>> trees;
    private Map<String, Map<String, PrintableTreeResults>> printTreeResults;
    private J48Batch loadedTree;
    private String conditionalPaths;
    private boolean useDateProperties;
    private Map<String,String> columnsAndType = Maps.newLinkedHashMap();
    private int depth = 1;
    private List<String> columnValue;
    private List<String> columnType;
    private List<Integer> missingIndicies;

    public DataCombinationResource(Config config, ObjectMapper objectMapper) {
        this.config = config;
        this.objectMapper = objectMapper;
        this.operationManager = new OperationManager(config, objectMapper);
    }

//    @GET
//    @Timed
//    @Path("/data/info")
//    public void getQueryInformation(@QueryParam("queryinfo") String queryinfo) {
//        cp.setJson(queryinfo);
//        Combination com = cp.getCombiningQuery();
//        DataClassifier dataClass = cp.getDataClassifier();
//        QueryConstructor qc = new QueryConstructor(config, com, dataClass);
//        qc.buildCombination().filterTable();
//        boolean tableExsits = config.dao.getReadData().tableExists("\'" + qc.getCombinedTableName() + "\'").size() == 0 ? false : true;
//        if(tableExsits){
//            config.dao.getLoadData().deleteTemporaryTable(qc.getCombinedTableName());
//        }
//        config.dao.getLoadData().createTemporaryTable(qc.toString(), qc.getCombinedTableName());
//        System.out.println("Table " + qc.getCombinedTableName() + " created.");
//    }

    @GET
    @Timed
    @Path("/data/info")
    public void getQueryInformation(@QueryParam("queryinfo") String queryinfo) throws IOException {
        operationManager.setJson(queryinfo);
        List<Map<String,TableBuilder>> operationOrder = operationManager.operationOrder();
        for(Map<String,TableBuilder> tb : operationOrder){
            for(Map.Entry<String,TableBuilder> table : tb.entrySet()) {
                config.dao.getLoadData().createTemporaryTable(table.getValue().getQuery(),table.getKey());
                logger.info("Table " + table.getKey() + " created.");
            }
        }
    }


    @POST
    @Timed
    @ApiOperation(
            value = "Export data set as an ARFF file",
            response = Map.class)
    @Path("/data/exportToArff")
    public void createArffFile(@QueryParam("table") String table, @QueryParam("outputName") String outputName){
        try {
            config.dao.getReadData().exportArff("SELECT * FROM " + table, "data/" + outputName +".arff");
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    @GET
    @Timed
    @ApiOperation(
            value = "Get available tables",
            response = List.class)
    @Path("/data/availableTables")
    public List<String> getTableList(){
        return config.dao.getReadData().getAvailableTables();
    }

    @GET
    @Timed
    @ApiOperation(
            value = "Get chosen data set metadata",
            response = List.class)
    @Path("/data/tableMetadata")
    public List<DataSet> getTableMetaData(@QueryParam("tableName") String tableName){
        return config.dao.getReadData().datasetInformation(tableName);
    }


    @GET
    @Timed
    @ApiOperation(
            value = "Get values of a chosen column",
            response = List.class)
    @Path("/data/columnvalues")
    public List<String> getLabelList(@QueryParam("tablename") String tableName,
                                     @QueryParam("columnname") String columnName){
        return config.dao.getReadData().selectLabels(tableName, columnName);
    }


    @GET
    @Timed
    @ApiOperation(
            value = "Get first 4 rows of a data set",
            response = List.class)
    @Path("/data/viewHeadOfTable")
    public List<Map<String,Object>> getHeadOfTable(@QueryParam("tables") String tableName){
        String query = new DataRead(config).buildTable(tableName, Lists.newArrayList()).getQuery();
        List<Map<String,Object>> head = config.dao.getReadData().executeQuery(query + " LIMIT 4;");
        logger.info(head.get(0).keySet().toString());
        return head;
    }


    @GET
    @Timed
    @ApiOperation(
            value = "Get column metadata for a data set",
            response = List.class)
    @Path("/data/tableStatistics")
    public List<ColumnDefinitions> getTableStats(@QueryParam("tables") String tableName){
        List<ColumnDefinitions> stats = config.dao.getReadData().datasetColumns(tableName);
        return stats;
    }

    @GET
    @Timed
    @ApiOperation(
            value = "Delete temporary tables")
    @Path("/data/removeTables")
    public void removeUserTables(){
        config.dao.getLoadData().removeCreatedTables();
    }


    public List<String> getTables(){
        List<DataSet> datasets = config.dao.getReadData().getdatasets();
        List<String> tableNames = Lists.newArrayList();
        for(DataSet ds : datasets){
            tableNames.add(ds.getName());
        }
        return tableNames;
    }

    @GET
    @Timed
    @ApiOperation(
            value = "Get available tables and the associated columns",
            response = Map.class)
    @Path("/data/datasetInfo")
    public Map<String,List<String>> getTablesAndColumns(){
        List<String> tables = getTables();
        Map<String,List<String>> tabCol = Maps.newLinkedHashMap();
        for(String table : tables) {
            tabCol.put(table,config.dao.getReadData().datasetColumNames(table));
        }
        return tabCol;
    }

//    @GET
//    @Timed
//    @Path("/data/load")
//    public void loadData(@QueryParam("datasetname") String datasetname,
//                         @QueryParam("description") String description,
//                         @QueryParam("source") String source,
//                         @QueryParam("location") String location){
//        String name = datasetname;
//    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/data/import")
    @ApiOperation(
            value = "Import csv file",
            response = DataSummary.class
    )
    public DataSummary importCsv(
            @FormDataParam("name") String name,
            @FormDataParam("description") String description,
            @FormDataParam("source") String source,
            @FormDataParam("createdYear") String createdYear,
            @FormDataParam("updated") String updated,
            @FormDataParam("whyCreated") String whyCreated,
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) throws IOException {
        logger.warn("Import data set: {}" + name);
        final DataSummary importStats = new DataImport(config).readInCsvBatch(name, description, source, createdYear, updated, whyCreated, uploadedInputStream);
        new DataRead(config).constructAndStorePermanentTable(name);
        logger.warn("Finished importing data set: {}, {}" + name, importStats);
        return importStats;
    }


    @GET
    @Timed
    @ApiOperation(
            value = "Write table as csv")
    @Path("/data/writetable")
    public void retrieveTable(@QueryParam("table") String table) throws IOException {
        String query = "SELECT * FROM " + table;
        String filename = "data/"+ table + "_output.csv";
        int count = config.dao.getReadData().exportCsv(query,filename,CSVFormat.DEFAULT);
        System.out.println("Number of lines: " + count);
    }

    @GET
    @Timed
    @ApiOperation(
            value = "Get column names of a data set",
            response = List.class)
    @Path("/data/columnnames")
    public List<String> getColumnNames(@QueryParam("table") String table){
        return new DataRead(config).getColumnNames(table);
    }

    @GET
    @Timed
    @ApiOperation(
            value = "Run a decision tree analysis",
            response = Map.class)
    @Path("/data/decisiontree")
    public Map<String,Map<String, PrintableTreeResults>> runDecisionTree(@QueryParam("table") String table,
                                @QueryParam("selectedFeatures") String selectedFeatures,
                                        @QueryParam("target") String target,
                                        @QueryParam("instance") String instance,
                                        @QueryParam("depth") String depth,
                                        @QueryParam("dateprop") boolean dateprop) throws Exception {
        boolean useTarget = !target.equals("All");
        this.useDateProperties = dateprop;
        String[] selectedCols = selectedFeatures.split(",");
        List<String> selCols = Lists.newArrayList(selectedCols);
        TrainTree model = useTarget ? new TrainTree(config, objectMapper, table, selCols, target, instance, dateprop) :
                new TrainTree(config, objectMapper, table, selCols, dateprop);

         this.columnType = Lists.newArrayList();
         this.columnValue = Lists.newArrayList();
         this.missingIndicies = Lists.newArrayList();

        for(int i = 0; i < selCols.size(); i++) {
            String sc = selCols.get(i);
            if(model.getColumnAndType().containsKey(sc)) {
                this.columnValue.add(sc);
                this.columnType.add(model.getColumnAndType().get(sc));
            }
            else{
                this.missingIndicies.add(i);
            }
        }

        this.columnsAndType.putAll(model.getColumnAndType());
        Map<String, Map<String, Label>> trainedModel = model.train(Integer.parseInt(depth),true);

        this.depth = Integer.parseInt(depth);
        trees = Maps.newHashMap();
        printTreeResults = Maps.newLinkedHashMap();
        trees.putAll(trainedModel);
        for(String col : trainedModel.keySet()){
            printTreeResults.put(col,Maps.newHashMap());
            for(String cls : trainedModel.get(col).keySet()){
                printTreeResults.get(col).put(cls,trainedModel.get(col).get(cls).getPrintableTreeResults());
            }
        }
        return printTreeResults;
    }

    @GET
    @Timed
    @ApiOperation(
            value = "Save a trained decision tree")
    @Path("/data/savedecisiontree")
    public void saveDecisionTree(){
        this.trees.values().stream().forEach(kv -> kv.values().stream().forEach(v -> {
            try {
                v.saveModel(config, this.columnValue, this.columnType, this.missingIndicies, this.useDateProperties);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }));
    }

    @GET
    @Timed
    @ApiOperation(
            value = "Load a trained decision tree",
            response = List.class)
    @Path("/data/loaddecisiontree")
    public List<String> loadDecisionTree(@QueryParam("modelName") String modelName)throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("data/models/" + modelName + ".model"));
        this.loadedTree = (J48Batch) ois.readObject();
        ois.close();

        String[] cols = config.dao.getReadData().getColumnsUsed(modelName).split(",");

        this.conditionalPaths = config.dao.getReadData().getConditionalPaths(modelName);

        StringBuilder sb = new StringBuilder("[");

        for(String col : cols) {
            sb.append("\"" + col + "\", ");
        }
        sb.deleteCharAt(sb.length()-2);
        sb.append("]");

        return Lists.newArrayList(sb.toString());
    }

    @GET
    @Timed
    @ApiOperation(
            value = "Run prediction logic for a trained model on a chosen data set",
            response = List.class)
    @Path("/data/rundecisiontree")
    public List<Map<String,String>> runDecisionTree(@QueryParam("dataset") String dataset, @QueryParam("modelName") String modelName) throws Exception {

        List<String> selectedCols = Lists.newArrayList(config.dao.getReadData().getColumnsUsed(modelName).split(","));
        List<String> columnTypes = Lists.newArrayList(config.dao.getReadData().getColumnTypes(modelName).split(","));
        String targetName = config.dao.getReadData().getTargetName(modelName);
        String targetValues = config.dao.getReadData().getTargetValues(modelName);
        boolean useDateProperties = Boolean.valueOf(config.dao.getReadData().getColumnOfInterestForModel(modelName,"use_date_properties"));

        Instances predictionSet = TrainTree.createDataForPrediction(dataset, selectedCols, columnTypes, config,
                modelName, targetName, targetValues, useDateProperties);

        List<Map<String,String>> instanceRow = Lists.newArrayList();
        for (int i = 0; i < predictionSet.numInstances(); i++) {
            //One row of data values
            Map<String,String> row = Maps.newHashMap();
            for(int j = 0; j < predictionSet.numAttributes() - 1; j++) {
                Attribute val = predictionSet.attribute(predictionSet.attribute(j).name());
                String output = val.type() == 0 ? Double.toString(predictionSet.instance(i).value(val)) : predictionSet.instance(i).stringValue(val);
                row.put(val.name(),output);
            }
            double predIndex = this.loadedTree.classifyInstance(predictionSet.instance(i));
//            predIndex = !useDateProperties ? predIndex + 9 : predIndex;
            Attribute at = predictionSet.classAttribute();
            row.put("predicted_"+at.name(),at.value((int) predIndex));
            instanceRow.add(row);
        }
        return instanceRow;
    }


    @GET
    @Timed
    @ApiOperation(
            value = "Number of rows in a chosen table",
            response = int.class)
    @Path("/data/tablerows")
    public int getRowCount(@QueryParam("table") String table){
        int count = config.dao.getReadData().getRowCount(table);
        return count;
    }


    @GET
    @Timed
    @ApiOperation(
            value = "View decision tree for a chosen feature and possibly specific value of feature",
            response = Tree.class)
    @Path("/data/decisiontree/jsontree")
    public Tree viewDecisionTree(@QueryParam("column") String column, @QueryParam("target") String target) {
        if(!target.equals("Multi-Class")){
            String tmp = column.toLowerCase();
            column = target.toLowerCase();
            target = tmp;
        }
        else{
            column = column.toLowerCase();
        }
        final Tree tree = trees.get(target).get(column).getTreeModel();
        return tree;
    }


    @GET
    @Timed
    @ApiOperation(
            value = "Statistics from decision tree analysis",
            response = Map.class)
    @Path("/data/decisiontree/results")
    public Map<String, Map<String, PrintableTreeResults>> decisionTreeResults(@QueryParam("column") String column) {
        return this.printTreeResults;
    }

    @GET
    @Timed
    @ApiOperation(
            value = "Retrieve trained decision tree in JSON",
            response = Tree.class)
    @Path("/data/retrievetree")
    public Tree retrieveTree(@QueryParam("modelName") String modelName)throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("data/models/" + modelName + ".model"));
        this.loadedTree = (J48Batch) ois.readObject();
        ois.close();

        return WekaTreeParser.parse(this.loadedTree.toString());
    }

    @GET
    @Timed
    @ApiOperation(
            value = "Get all learned conditional paths for each classification",
            response = String.class)
    @Path("/data/conditionalPaths")
    @Produces({MediaType.APPLICATION_JSON})
    public String getConditionalPaths(@QueryParam("modelName") String modelName){
        return config.dao.getReadData().getConditionalPaths(modelName);
    }

    @GET
    @Timed
    @ApiOperation(
            value = "View all available saved model names",
            response = List.class)
    @Path("/data/availableModels")
    public List<String> getModelList(){
        return config.dao.getReadData().getAvailableModels();
    }

}
