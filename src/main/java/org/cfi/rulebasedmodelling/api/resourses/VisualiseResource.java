package org.cfi.rulebasedmodelling.api.resourses;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import org.cfi.rulebasedmodelling.api.Config;
import org.cfi.rulebasedmodelling.datamanagement.TableData;
import org.cfi.rulebasedmodelling.time.TimeLine;
import org.cfi.rulebasedmodelling.time.TimeValue;
import org.cfi.rulebasedmodelling.util.StatReduce;
import org.joda.time.Period;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Map;

@Path("/visualise")
@Api("/visualise")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class VisualiseResource {

    Config config;
    ObjectMapper objectMapper;

    public VisualiseResource(Config config, ObjectMapper objectMapper) {
        this.config = config;
        this.objectMapper = objectMapper;
    }

    private TimeLine<TableData> getTableData(){
        List<Map<String, Object>> data = config.dao.getReadData().executeQuery("SELECT * FROM rainfall_and_events_filtered");
        TimeLine<TableData> timeline = new TimeLine<>();
        for(Map<String, Object> q : data){
            timeline.add(new TableData(q));
        }
        return timeline;
    }

    @GET
    @Timed
    @Path("/data/retrievetable")
    public TimeLine<TimeValue> getData(){
        TimeLine<TableData> data = getTableData();
        return data.resample(Period.weeks(1), d -> isDouble(d.getData().get("fatalities")), StatReduce.SUM);
    }

    private int isDouble(Object val){
        double newval = (double) val;
        return (int) newval;
    }


}
