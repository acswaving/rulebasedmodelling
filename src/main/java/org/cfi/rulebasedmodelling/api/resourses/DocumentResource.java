package org.cfi.rulebasedmodelling.api.resourses;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.cfi.rulebasedmodelling.api.Config;
import org.cfi.rulebasedmodelling.postgres.dao.JsonDocDao;
import org.cfi.rulebasedmodelling.postgres.dao.TextDocDao;
import org.cfi.rulebasedmodelling.postgres.datamanagement.JsonDoc;
import org.cfi.rulebasedmodelling.postgres.datamanagement.JsonDocMapper;
import org.cfi.rulebasedmodelling.postgres.datamanagement.TextDoc;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.hibernate.validator.constraints.Length;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Document (text and json) storage
 * @author Arvid Halma
 * @version 23-11-2015
 */

@Path("/doc")
@Api("/doc")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class DocumentResource {

    private Config config;
    private final Logger logger = LoggerFactory.getLogger(DocumentResource.class);

    private TextDocDao textDocDao;
    private JsonDocDao jsonDocDao;

    public DocumentResource(Config configuration) {
        this.config = configuration;
        textDocDao = this.config.dao.getTextDocDao();
        jsonDocDao = this.config.dao.getJsonDocDao();
    }



    /////////////// TextDoc /////////////// 

    @GET
    @Path("/textdoc/id/{id}")
    @ApiOperation(
            value = "Get text document by id",
            response = TextDoc.class)
    public TextDoc getTextDoc(@PathParam("id") String id) {
        return textDocDao.getById(id);
    }

    @GET
    @Path("/textdoc/id/{id}/body")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Get text document body/content by id",
            response = String.class)
    public String getTextDocBody(@PathParam("id") String id) {
        return textDocDao.getById(id).getBody();
    }

    @GET
    @Path("/textdoc/{type}/meta")
    @ApiOperation(
            value = "Get text document meta information by id",
            notes = "Everything, but the body",
            response = TextDoc.class,
            responseContainer = "List"
    )
    public List<TextDoc> getTextDocMetas(@PathParam("type") String type) {
        return textDocDao.getAllMetaLastUpdated(type);
    }

    @GET
    @Path("/textdoc/{type}/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Get last updated text document content/body by type and name",
            response = TextDoc.class)
    public String getLastTextDoc(@PathParam("type") String type, @PathParam("name") String name) {
        final TextDoc doc = textDocDao.getLastUpdatedByName(type, name);
        if (doc == null) {
            throw new WebApplicationException(404);
        }
        return doc.getBody();
    }

    @GET
    @Path("/textdoc/{type}")
    @ApiOperation(
            value = "Get all text documents by type",
            response = TextDoc.class,
            responseContainer = "List")
    public List<TextDoc> getTextDocs(@PathParam("type") String type) {
        return textDocDao.getAllLastUpdated(type);
    }

    @GET
    @Path("/textdoc/{type}/{name}/meta")
    @ApiOperation(
            value = "Get text document meta information for a given type and name",
            notes = "I.e. retrieve all versions. Everything, but the body",
            response = TextDoc.class,
            responseContainer = "List"
    )
    public List<TextDoc> getTextDocVersionMetas(@PathParam("type") String type, @PathParam("name") String name) {
        return textDocDao.getAllMetas(type, name);
    }

    @DELETE
    @Path("/textdoc/{type}/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Delete all text document versions by type and name")
    public void deleteTextDoc(@PathParam("type") String type, @PathParam("name") @NotNull @Length(min = 1, max = 64) String name) {
        textDocDao.deleteByName(type, name);
    }

    @GET
    @Path("/textdoc/{type}/name")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Get text document names for a given type",
            response = String.class,
            responseContainer = "List"
    )
    public List<String> getTextDocNames(@PathParam("type") String type) {
        return textDocDao.getAllNames(type);
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/textdoc/{type}/{name}")
    @ApiOperation(
            value = "Save a text document given a type and name",
            notes = "It won't overwrite possible earlier versions, but create another version if the same name is used."
    )
    public void saveTextDoc(@PathParam("type") String type, @PathParam("name") @NotNull @Length(min = 1, max = 64) String name, @FormDataParam("body") String body) {
        textDocDao.upsert(new TextDoc().setName(name).setType(type).setBody(body));
    }

    /////////////// JsonDoc /////////////// 

    @GET
    @Path("/jsondoc/id/{id}")
    @ApiOperation(
            value = "Get JSON document by id",
            response = JsonDoc.class)
    public JsonDoc getJsonDoc(@PathParam("id") String id) {
        return jsonDocDao.getById(id);
    }

    @GET
    @Path("/jsondoc/id/{id}/body")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Get JSON document body/content by id",
            response = String.class)
    public Map<String, Object> getJsonDocBody(@PathParam("id") String id) {
        return jsonDocDao.getById(id).getBody();
    }

    @GET
    @Path("/jsondoc/{type}/meta")
    @ApiOperation(
            value = "Get JSON document meta information by id",
            notes = "Everything, but the body",
            response = JsonDoc.class,
            responseContainer = "List"
    )
    public List<JsonDoc> getJsonDocMetas(@PathParam("type") String type) {
        return jsonDocDao.getAllMetaLastUpdated(type);
    }

    @GET
    @Path("/jsondoc/{type}/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Get last updated JSON document content/body by type and name",
            response = JsonDoc.class)
    public Map<String, Object> getLastJsonDoc(@PathParam("type") String type, @PathParam("name") String name) {
        final JsonDoc doc = jsonDocDao.getLastUpdatedByName(type, name);
        if (doc == null) {
            throw new WebApplicationException(404);
        }
        return doc.getBody();
    }

    @GET
    @Path("/jsondoc/{type}")
    @ApiOperation(
            value = "Get all JSON documents by type",
            response = JsonDoc.class,
            responseContainer = "List")
    public List<JsonDoc> getJsonDocs(@PathParam("type") String type) {
        return jsonDocDao.getAllLastUpdated(type);
    }

    @GET
    @Path("/jsondoc/{type}/{name}/meta")
    @ApiOperation(
            value = "Get JSON document meta information for a given type and name",
            notes = "I.e. retrieve all versions. Everything, but the body",
            response = JsonDoc.class,
            responseContainer = "List"
    )
    public List<JsonDoc> getJsonDocVersionMetas(@PathParam("type") String type, @PathParam("name") String name) {
        return jsonDocDao.getAllMetas(type, name);
    }

    @DELETE
    @Path("/jsondoc/{type}/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Delete all JSON document versions by type and name")
    public void deleteJsonDoc(@PathParam("type") String type, @PathParam("name") @NotNull @Length(min = 1, max = 64) String name) {
        jsonDocDao.deleteByName(type, name);
    }

    @GET
    @Path("/jsondoc/{type}/name")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Get JSON document names for a given type",
            response = String.class,
            responseContainer = "List"
    )
    public List<String> getJsonDocNames(@PathParam("type") String type) {
        return jsonDocDao.getAllNames(type);
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/jsondoc/{type}/{name}")
    @ApiOperation(
            value = "Save a JSON document given a type and name",
            notes = "It won't overwrite possible earlier versions, but create another version if the same name is used."
    )
    public void saveJsonDoc(@PathParam("type") String type, @PathParam("name") @NotNull @Length(min = 1, max = 64) String name, @FormDataParam("body") String body) {
        final LinkedHashMap<String, Object> bodyMap = JsonDocMapper.readJson(body);
        jsonDocDao.upsert(new JsonDoc().setName(name).setType(type).setBody(bodyMap));
    }

}