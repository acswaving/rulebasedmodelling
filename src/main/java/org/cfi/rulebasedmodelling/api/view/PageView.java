package org.cfi.rulebasedmodelling.api.view;

import io.dropwizard.views.View;


public class PageView extends View {

    private final String contentFtl;

    public PageView(String contentFtl) {
        super("page.ftl");
        this.contentFtl = contentFtl;
    }

    public String getContentFtl() {
        return contentFtl;
    }
}
