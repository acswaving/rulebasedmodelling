package org.cfi.rulebasedmodelling.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import org.cfi.rulebasedmodelling.connections.externalapi.WfpConfig;
import org.cfi.rulebasedmodelling.postgres.dao.Dao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class Config extends Configuration {

    private final Logger logger = LoggerFactory.getLogger(Config.class);

    public Dao dao;

    @JsonProperty
    public boolean prettyPrintJsonResponse;

    @Valid
    @NotNull
    private DataSourceFactory database = new DataSourceFactory();

    @JsonProperty("swagger")
    public SwaggerBundleConfiguration swaggerBundleConfiguration;

    @JsonProperty
    public boolean useElasticSearch;

    public boolean isUseElasticSearch() {
        return useElasticSearch;
    }

    @JsonProperty("database")
    public void setDataSourceFactory(DataSourceFactory factory) {
        this.database = factory;
    }

    @JsonProperty("database")
    public DataSourceFactory getDataSourceFactory() {
        return database;
    }

    public SwaggerBundleConfiguration getSwaggerBundleConfiguration() {
        return swaggerBundleConfiguration;
    }

    @JsonProperty("wfpApi")
    public WfpConfig wfpApiConfig;
}
