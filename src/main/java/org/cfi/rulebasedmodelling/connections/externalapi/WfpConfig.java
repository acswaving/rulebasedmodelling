package org.cfi.rulebasedmodelling.connections.externalapi;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WfpConfig {

    @JsonProperty
    public String externalendpoint;

    @JsonProperty
    public String datavizendpoint;

    @JsonProperty
    public String key;
}
