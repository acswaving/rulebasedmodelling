package org.cfi.rulebasedmodelling.postgres.dao;

import com.google.common.collect.Sets;
import org.cfi.rulebasedmodelling.postgres.datamanagement.*;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.sqlobject.SqlObject;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlBatch;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public interface LoadData extends SqlObject {

    Logger logger = LoggerFactory.getLogger(LoadData.class);

    @SqlUpdate(//language=sql
            "INSERT INTO dataset(id, name, last_updated, description, source) VALUES(:id, :name, CAST(:last_updated AS TIMESTAMPTZ), :description, :source) " +
            "ON CONFLICT (id) " +
            "DO UPDATE SET (name, last_updated, description, source) = (:name, CAST(:last_updated AS TIMESTAMPTZ), :description, :source) " +
            "WHERE dataset.id = :id;")
    void upsert(@BindBean DataSet ds);

    @SqlUpdate(//language=sql
            "INSERT INTO columndefinitions(columnid, name, description, type, datasetid) VALUES(:columnid, :name, :description, :type, :datasetid) " +
                    "ON CONFLICT (columnid) " +
                    "DO UPDATE SET (name, description, type, datasetid) = (:name, :description, :type, :datasetid) " +
                    "WHERE columndefinitions.columnid = :columnid;")
    void upsert(@BindBean ColumnDefinitions cd);

    @SqlUpdate(//language=sql
            "UPDATE columndefinitions SET description = :description " +
                    "WHERE columndefinitions.columnid = :columnid;")
    void upsertDescription(@BindBean ColumnDefinitions cd);

    @SqlUpdate( //language=sql
            "INSERT INTO geopoint(rowid, columnid, geopoint) VALUES(:rowid, :columnid,  (CASE WHEN :latitude = 0.0 THEN null ELSE ST_GeomFromText('POINT(' || :longitude || ' ' || :latitude || ')',4326) END)) " +
            "ON CONFLICT (rowid, columnid) " +
            "DO UPDATE SET geopoint = (CASE WHEN :latitude = 0.0 THEN null ELSE ST_GeomFromText('POINT(' || :longitude || ' ' || :latitude || ')',4326) END) " +
            "WHERE (geopoint.rowid, geopoint.columnid) = (:rowid, :columnid);")
    void upsert(@BindBean GeoPoints gp);

    @SqlUpdate( //language=sql
            "INSERT INTO datetimevalues(rowid, columnid, value) VALUES(:rowid, :columnid, CAST(:value AS TIMESTAMPTZ)) " +
                    "ON CONFLICT (rowid, columnid) " +
                    "DO UPDATE SET value = CAST(:value AS TIMESTAMPTZ) " +
                    "WHERE (datetimevalues.rowid, datetimevalues.columnid) = (:rowid, :columnid);")
    void upsert(@BindBean DateTimeValues dt);

    @SqlUpdate( //language=sql
            "INSERT INTO doublevalues(rowid, columnid, value) VALUES(:rowid, :columnid, :value) " +
                    "ON CONFLICT (rowid, columnid) " +
                    "DO UPDATE SET value = :value " +
                    "WHERE (doublevalues.rowid, doublevalues.columnid) = (:rowid, :columnid);")
    void upsert(@BindBean DoubleValues dv);

    @SqlUpdate( //language=sql
            "INSERT INTO stringvalues(rowid, columnid, value) VALUES(:rowid, :columnid, :value) " +
                    "ON CONFLICT (rowid, columnid) " +
                    "DO UPDATE SET value = :value " +
                    "WHERE (stringvalues.rowid, stringvalues.columnid) = (:rowid, :columnid);")
    void upsert(@BindBean StringValues sv);

    @SqlBatch( //language=sql
            "INSERT INTO geopoint(rowid, columnid, geopoint) VALUES(:rowid, :columnid,  (CASE WHEN :latitude = 0.0 THEN null ELSE ST_GeomFromText('POINT(' || :longitude || ' ' || :latitude || ')',4326) END)) " +
            "ON CONFLICT (rowid, columnid) " +
            "DO UPDATE SET geopoint = (CASE WHEN :latitude = 0.0 THEN null ELSE ST_GeomFromText('POINT(' || :longitude || ' ' || :latitude || ')',4326) END) " +
            "WHERE (geopoint.rowid, geopoint.columnid) = (:rowid, :columnid);")
    void upsertGeoPoints(@BindBean Collection<GeoPoints> gp);

    @SqlBatch( //language=sql
            "INSERT INTO datetimevalues(rowid, columnid, value) VALUES(:rowid, :columnid, CAST(:value AS TIMESTAMPTZ)) " +
                    "ON CONFLICT (rowid, columnid) " +
                    "DO UPDATE SET value = CAST(:value AS TIMESTAMPTZ) " +
                    "WHERE (datetimevalues.rowid, datetimevalues.columnid) = (:rowid, :columnid);")
    void upsertDateTimeValues(@BindBean Collection<DateTimeValues> dt);

    @SqlBatch( //language=sql
            "INSERT INTO doublevalues(rowid, columnid, value) VALUES(:rowid, :columnid, :value) " +
                    "ON CONFLICT (rowid, columnid) " +
                    "DO UPDATE SET value = :value " +
                    "WHERE (doublevalues.rowid, doublevalues.columnid) = (:rowid, :columnid);")
    void upsertDoubleValues(@BindBean Collection<DoubleValues> dv);

    @SqlBatch( //language=sql
            "INSERT INTO stringvalues(rowid, columnid, value) VALUES(:rowid, :columnid, :value)\n" +
                    "ON CONFLICT (rowid, columnid)\n" +
                    "DO UPDATE SET value = :value\n" +
                    "WHERE (stringvalues.rowid, stringvalues.columnid) = (:rowid, :columnid);")
    void upsertStringValues(@BindBean Collection<StringValues> sv);



    default void createTemporaryTable(String query, String name){
        name = name.contains(" ") ? name.replace(" ", "_") : name;
        Handle handle = getHandle();
        if(query.contains("ALTER")) {
            logger.info("Adding column to table " + name);
            logger.info(query);
            int count = handle.execute(query);
            logger.info("rows affected: " + count);
        }
        else {
            logger.info("Creating temp table...");
            logger.info("First check if table exists");
            if(tableExists(name)){
                deleteTemporaryTable(name);
            }
            String totalQuery = "CREATE UNLOGGED TABLE " + name + " AS (" + query + ");";
            logger.info(totalQuery);
            int count = handle.execute(totalQuery);
            logger.info("rows affected: " + count);
        }
    }

    default boolean tableExists(String dataset){
        logger.info("Checking if " + dataset + " exists...");
        Handle handle = getHandle();
        List<String> tempTables = handle.createQuery("SELECT * FROM pg_catalog.pg_class WHERE relkind = 'r' AND relowner in (SELECT relowner FROM pg_catalog.pg_class WHERE relname = 'dataset');").mapTo(String.class).list();
        Set filterSet = Sets.newHashSet("databasechangelog", "databasechangeloglock", "dataset",
                "doublevalues", "stringvalues", "columndefinitions", "datetimevalues");
        tempTables = tempTables.stream().filter(tt -> !filterSet.contains(tt)).collect(Collectors.toList());
        boolean isExist = tempTables.contains(dataset);
        logger.info(dataset + " exists: " + String.valueOf(isExist));
        return isExist;
    }

    default void removeCreatedTables(){
        logger.info("Removing User Created Tables");
        Handle handle = getHandle();
        List<String> tempTables = handle.createQuery("SELECT * FROM pg_catalog.pg_class WHERE relkind = 'r' AND relowner in (SELECT relowner FROM pg_catalog.pg_class WHERE relname = 'dataset');").mapTo(String.class).list();
        Set filterSet = Sets.newHashSet("databasechangelog", "databasechangeloglock", "dataset",
                "doublevalues", "stringvalues", "columndefinitions", "datetimevalues");
        tempTables = tempTables.stream().filter(tt -> !filterSet.contains(tt)).collect(Collectors.toList());
        List<String> permanentTables = handle.createQuery("SELECT name FROM dataset").mapTo(String.class).list();
        for(String table : tempTables){
            if(!permanentTables.contains(table)) {
                handle.execute("DROP TABLE " + table);
            }
        }
    }

    default void deleteTemporaryTable(String name){
        Handle handle = getHandle();
        String totalQuery = "DROP TABLE " + name + ";";
        logger.info("Dropping table" + name);
        handle.execute(totalQuery);
    }

    default void deleteExistingTable(String tablename){
        Handle handle = getHandle();
        String totalQuery = "DELETE FROM dataset WHERE name =\'" + tablename + "\';";
        handle.execute(totalQuery);
    }

    default void deleteCreatedModel(String modelName){
        Handle handle = getHandle();
        String totalQuery = "DELETE FROM stringvalues WHERE rowid IN (SELECT rowid FROM stringvalues " +
                "WHERE value = \'" + modelName + "\') AND columnid IN " +
                "(SELECT columnid FROM columndefinitions WHERE datasetid IN " +
                "(SELECT id FROM dataset where name = 'created_models'));";
        handle.execute(totalQuery);
    }

    default void runQuery(String query){
        Handle handle = getHandle();
        handle.execute(query);
    }

}
