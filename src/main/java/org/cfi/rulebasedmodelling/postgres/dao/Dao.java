package org.cfi.rulebasedmodelling.postgres.dao;


/**
 * Container for all Data Access Object interfaces.
 */
public class Dao {

    private final LoadData loadData;
    private final ReadData readData;
    private final TextDocDao textDocDao;
    private final JsonDocDao jsonDocDao;

    public Dao(LoadData loadData, ReadData readData, TextDocDao textDocDao, JsonDocDao jsonDocDao) {
        this.loadData = loadData;
        this.readData = readData;
        this.textDocDao = textDocDao;
        this.jsonDocDao = jsonDocDao;
    }

    public LoadData getLoadData(){
        return loadData;
    }

    public ReadData getReadData() {
        return readData;
    }

    public TextDocDao getTextDocDao() {
        return textDocDao;
    }

    public JsonDocDao getJsonDocDao() {
        return jsonDocDao;
    }

}
