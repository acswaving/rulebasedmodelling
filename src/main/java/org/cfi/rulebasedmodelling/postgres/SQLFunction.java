package org.cfi.rulebasedmodelling.postgres;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public enum SQLFunction {

    SUM{
        @Override
        public String insertInfrontOfQuery(List<String> columns) {
            return "SELECT " + String.join(", ",columns) +  " FROM (";
        }

        @Override
        public String insertInfrontOfQuery(List<String> columns, List<String> groupby, Set<String> groupLabels) {
            return "SELECT " + getColumnsActedON(SUM.toString(), columns) + getGroupByString(groupby, groupLabels) +  " FROM (";
        }

        @Override
        public String appendAtEndOfQuery(List<String> groupLabels) {
            return ") agtab GROUP BY " + getGroupByLabel(groupLabels);
        }
    },COUNT{
        @Override
        public String insertInfrontOfQuery(List<String> columns) {
            return "SELECT " + String.join(", ",columns) +  " FROM (";
        }

        @Override
        public String insertInfrontOfQuery(List<String> columns, List<String> groupby, Set<String> groupLabels) {
            return "SELECT " + getColumnsActedON(COUNT.toString(), columns) + getGroupByString(groupby, groupLabels) +  " FROM (";
        }

        @Override
        public String appendAtEndOfQuery(List<String> groupLabels) {
            return ") agtab GROUP BY " + getGroupByLabel(groupLabels);
        }
    },MAX{
        @Override
        public String insertInfrontOfQuery(List<String> columns) {
            return "SELECT " + String.join(", ",columns) +  " FROM (";
        }

        @Override
        public String insertInfrontOfQuery(List<String> columns, List<String> groupby, Set<String> groupLabels) {
            return null;
        }

        @Override
        public String appendAtEndOfQuery(List<String> groupLabels) {
            return null;
        }
    },MIN{
        @Override
        public String insertInfrontOfQuery(List<String> columns) {
            return "SELECT " + String.join(", ",columns) +  " FROM (";
        }

        @Override
        public String insertInfrontOfQuery(List<String> columns, List<String> groupby, Set<String> groupLabels) {
            return null;
        }

        @Override
        public String appendAtEndOfQuery(List<String> groupLabels) {
            return null;
        }
    },AVG{
        @Override
        public String insertInfrontOfQuery(List<String> columns) {
            return "SELECT " + String.join(", ",columns) +  " FROM (";
        }

        @Override
        public String insertInfrontOfQuery(List<String> columns, List<String> groupby, Set<String> groupLabels) {
            return "SELECT " + getColumnsActedON(AVG.toString(), columns) + getGroupByString(groupby, groupLabels) +  " FROM (";
        }

        @Override
        public String appendAtEndOfQuery(List<String> groupLabels) {
//            return ") agtab GROUP BY " + getGroupByLabel(groupLabels);
            return ") agtab GROUP BY " + String.join(", ",groupLabels);
        }
    },NONE{
        @Override
        public String insertInfrontOfQuery(List<String> columns) {
            return "SELECT " + String.join(", ",columns) +  " FROM (";
        }

        @Override
        public String insertInfrontOfQuery(List<String> columns, List<String> groupby, Set<String> groupLabels) {
            return "";
        }

        @Override
        public String appendAtEndOfQuery(List<String> groupLabels) {
            return "";
        }
    };

    abstract public String insertInfrontOfQuery(List<String> columns);
    abstract public String insertInfrontOfQuery(List<String> columns, List<String> groupby, Set<String> groupLabels);
    abstract public String appendAtEndOfQuery(List<String> groupLabels);

    public String getGroupByString(List<String> groupby, Set<String> groupLabels){
        String groups = "";
        List<String> gl = new ArrayList(groupLabels);
        for(int i = 0; i < groupby.size() - 1; i++){
//            groups += "agtab.";
            groups += groupby.get(i);
            groups += " AS ";
            groups += gl.get(i);
            groups += ", ";
        }
//        groups += "agtab.";
        groups += groupby.get(groupby.size() - 1);
        groups += " AS ";
        groups += gl.get(groupby.size() - 1);
//        for(int i = 0; i < groupby.size() - 1; i++){
//            groups += "agtab.";
//            groups += groupby.get(i);
//            groups += ", ";
//        }
//        groups += "agtab.";
//        groups += groupby.get(groupby.size() - 1);
        return groups;
    }

    public String getGroupByLabel(List<String> groupLabels){
        String groups = "";
        List<String> groupby = new ArrayList(groupLabels);
        for(int i = 0; i < groupby.size() - 1; i++){
//            groups += "agtab.";
            groups += groupby.get(i);
            groups += ", ";
        }
//        groups += "agtab.";
        groups += groupby.get(groupby.size() - 1);
        return groups;
    }

    public String getColumnsActedON(String function, List<String> actedON){
        String acted = "";

//        for(int i = 0; i < actedON.size(); i++){
//            acted += function;
//            acted += "(";
//            acted += actedON.get(i);
//            acted += ") AS ";
//            acted += actedON.get(i);
//            acted += "_";
//            acted += function.toLowerCase();
//            acted += ", ";
//        }
        for(int i = 0; i < actedON.size(); i++){
            acted += function;
            acted += "(";
            acted += "agtab.";
            acted += actedON.get(i);
            acted += ") AS ";
            acted += actedON.get(i);
            acted += ", ";
        }
        return acted;
    }

}
