package org.cfi.rulebasedmodelling.postgres.datamanagement;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;
import org.joda.time.DateTime;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DataSetMapper implements RowMapper<DataSet> {

    @Override
    public DataSet map(ResultSet rs, StatementContext ctx) throws SQLException{
        return new DataSet()
                .setId(rs.getLong("id"))
                .setName(rs.getString("name"))
                .setLast_updated(new DateTime(rs.getTimestamp("last_updated")))
                .setDescription(rs.getString("description"))
                .setSource(rs.getString("source"));
    }
}
