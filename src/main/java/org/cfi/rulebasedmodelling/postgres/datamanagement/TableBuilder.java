package org.cfi.rulebasedmodelling.postgres.datamanagement;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.cfi.rulebasedmodelling.datamanagement.Query.CombinationItem;
import org.cfi.rulebasedmodelling.datamanagement.Query.DataClassifier;
import org.cfi.rulebasedmodelling.datamanagement.Query.RuleClassifier;
import org.cfi.rulebasedmodelling.postgres.SQLFunction;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TableBuilder {

    private StringBuilder query;
    private AtomicInteger numberOfJoins = new AtomicInteger();
    private AtomicInteger numberOfConditions = new AtomicInteger();
    private List<String> columnNames = Lists.newArrayList();

    public TableBuilder(Table initialTable, List<ColumnDefinitions> columnDefs){
        this.query = new StringBuilder(constructColumnNamesInOrder(columnDefs));
        this.query.append(initialTable.generateQuery());
        this.query.append(")");
    }

    public TableBuilder(){
        this.query = new StringBuilder();
    }

    public TableBuilder(String query){
        this.query = new StringBuilder(query);
    }

    public TableBuilder(List<ColumnDefinitions> columnDefs){
        this.columnNames = columnDefs.stream().map(getNames).collect(Collectors.toList());
        List<Table> availableTables = generateOrderOfTables(columnDefs);
        if(!availableTables.isEmpty()) {
            Table firstTableType = availableTables.get(0);
            List<String> columnNumbers = getColumnNumbers(firstTableType);
            this.query = new StringBuilder(firstTableType.generateQuery());
            this.query.append(")");
//            this.query.append(" tab" +  numberOfJoins.getAndIncrement()); // not sure
            for (int i = 1; i < availableTables.size(); i++) {
                selfjoin(availableTables.get(i), columnNumbers);
            }
            this.query.insert(0,constructColumnNamesInOrder(columnDefs));
            if(availableTables.size() < 2) this.query.append("bla");
        }
        else{
            System.out.println("ERROR: No table with these columns exists.");
            this.query = new StringBuilder();
        }
    }

    private List<String> getColumnNumbers(Table table){
        List<String> colTracker = Lists.newArrayList();
        for(long col : table.getColumns()){
            colTracker.add("col" + col);
        }
        return colTracker;
    }

    private Function<ColumnDefinitions,Long> getIds =  x -> x.getColumnid();
    private Function<ColumnDefinitions,String> getNames =  x -> x.getName();

    private List<Table> generateOrderOfTables(List<ColumnDefinitions> columnDefs){
        List<Long> doubleCols = columnDefs.stream().filter(cd -> cd.getType().equals("numeric"))
                .map(getIds).collect(Collectors.toList());
        List<Long> stringCols = columnDefs.stream().filter(cd -> cd.getType().equals("string"))
                .map(getIds).collect(Collectors.toList());
        List<Long> datetimeCols = columnDefs.stream().filter(cd -> cd.getType().equals("datetime"))
                .map(getIds).collect(Collectors.toList());
        List<Table> possibleTables = Lists.newArrayList();
        if(!doubleCols.isEmpty()) possibleTables.add(new TableType.DoubleTable(doubleCols));
        if(!stringCols.isEmpty()) possibleTables.add(new TableType.StringTable(stringCols));
        if(!datetimeCols.isEmpty()) possibleTables.add(new TableType.DateTimeTable(datetimeCols));
        return possibleTables;
    }

    private void selfjoin(Table table, List<String> columnTracker){
        int joinNum = numberOfJoins.getAndIncrement();
        if(joinNum > 1) {
            String cols = "tab"+ (joinNum - 1) +".rowid, ";
            for(int i = 0; i < columnTracker.size(); i++){
                cols += columnTracker.get(i);
                if(i != columnTracker.size() - 1) {
                    cols += ", ";
                }
            }
            this.query.insert(0,"SELECT " + cols + " FROM (");
            this.query.append(")");
        }
        int nextNum = numberOfJoins.getAndIncrement();
        this.query.append(" tab");
        this.query.append(joinNum);
        this.query.append(" FULL OUTER JOIN ");
        this.query.append("(");
        this.query.append(table.generateQuery());
        this.query.append(")");
        this.query.append(" tab");
        this.query.append(nextNum);
        this.query.append(" ON ");
        this.query.append("tab");
        this.query.append(joinNum);
        this.query.append(".rowid");
        this.query.append(" = ");
        this.query.append(" tab");
        this.query.append(nextNum);
        this.query.append(".rowid");
        columnTracker.addAll(getColumnNumbers(table));

    }

    public TableBuilder aggregateBy(SQLFunction function, List<String> columns, Map<String, String> groupby, String tableName){
        List<String> columnsAvailableToGroupBy = getAvailableColumnsToGroup(tableName, groupby);
        this.columnNames = Lists.newArrayList(); /** this empties the column names**/
        columns.removeAll(removePrefix(groupby.keySet()));
        List<String> cols = addColumnsWithTableName(columns,groupby,tableName);
        this.columnNames.addAll(columnsAvailableToGroupBy);
        this.query.insert(0,function.insertInfrontOfQuery(cols,columnsAvailableToGroupBy, removePrefix(groupby.keySet())));
        this.query.append(function.appendAtEndOfQuery(columnsAvailableToGroupBy));
        return this;
    }

    private List<String> addColumnsWithTableName(List<String> columns, Map<String,String> groupby, String tableName){
        List<String> cols = Lists.newArrayList();
        for(String column : columns) {
            if(!removePrefix(groupby.keySet()).contains(column)) {
                this.columnNames.add(tableName + "_" + column);
                cols.add(tableName + "_" + column);
            }
        }
        return cols;
    }

    private List<String> getAvailableColumnsToGroup(String tableName, Map<String,String> groups){
        List<String> columnsAvailableToGroupBy = Lists.newArrayList();
        for(String gb : groups.keySet()){
            if(this.columnNames.contains(removePrefix(gb)) || this.columnNames.isEmpty()) {
                columnsAvailableToGroupBy.add(tableName + "_" + groups.get(gb));
            }
        }
        return columnsAvailableToGroupBy;
    }

    private Set<String> removePrefix(Set<String> bys){
        Set<String> cleanedBys = Sets.newHashSet();
        for(String b : bys){
            b = removePrefix(b);
            cleanedBys.add(b);
        }
        return cleanedBys;
    }

    private String removePrefix(String by){
            if(by.contains("join_")){
                by = by.replace("join_","");
            }
        return by;
    }

    public TableBuilder aggregateBy(boolean isAggregate, SQLFunction function, List<String> columns, Map<String, String> groupby, String tableName){
        if(isAggregate) {
            return aggregateBy(function, columns, groupby, tableName);
        }
        else{
            return this;
        }
    }

    public TableBuilder aggregateBy(CombinationItem combinationItem){
        if(combinationItem.isAggregate()) {
            this.query.insert(0,combinationItem.getAggregateFunction().insertInfrontOfQuery(combinationItem.getColumnsWhenAggregating()));
            this.query.append(combinationItem.getAggregateFunction().appendAtEndOfQuery(combinationItem.getCleanedGroupByColumns()));
        }
        return this;
    }

    public TableBuilder endOfQuery(){
        this.query.append(";");
        return this;
    }

    public String getQuery(){
        return this.query.toString();
    }

    public StringBuilder getBuilder(){
        return this.query;
    }

    private String constructColumnNamesInOrder(List<ColumnDefinitions> columnDefs){
        StringBuilder columns = new StringBuilder("SELECT ");
        int colNum = 0;
        for (ColumnDefinitions cd : columnDefs) {
            String columnQuery = "col" + Long.toString(cd.getColumnid()) + " AS " + cd.getName();
            columns.append(columnQuery);
            if (colNum != columnDefs.size() - 1) {
                columns.append(", ");
            } else {
                columns.append(" ");
            }
            colNum++;
        }
        columns.append("FROM (");
        return columns.toString();
    }

    public List<String> getColumnNames(){
        return this.columnNames;
    }


    public <T>TableBuilder conditional(String column, T value, String relational){
        if(numberOfConditions.getAndIncrement() == 0) {
            this.query.insert(0,"SELECT * FROM (");
            this.query.append(") blah WHERE ");
        }
        this.query.append(column + " ");
        this.query.append(relational);
        if(value.getClass() == String.class){
            this.query.append(" " + "\'" + value + "\'");
        }
        else {
            this.query.append(" " + value);
        }
        return this;
    }

    public TableBuilder conditional(String condition, boolean isJoin){
        if(numberOfConditions.getAndIncrement() == 0) {
            this.query.insert(0,"SELECT * FROM (");
            this.query.append(") tableToConditionOn WHERE ");
        }
        this.query.append(condition);
        return this;
    }

    public TableBuilder conditional(String condition, String table){
        if(numberOfConditions.getAndIncrement() == 0) {
            this.query.insert(0,"SELECT * FROM (");
            this.query.append(") tableToConditionOn WHERE ");
//            this.query.append(" WHERE ");
//            this.query.append("SELECT * FROM " + table + " WHERE ");
        }
        this.query.append(condition);
        return this;
    }

    public TableBuilder derivedDataFilters(RuleClassifier ruleClassifier){
        this.query.append("ALTER TABLE " + ruleClassifier.getTable() + " ADD COLUMN " +
                ruleClassifier.getClassifier() + " " + ruleClassifier.getType() + ";");
        if(!ruleClassifier.getConditionals().containsValue(null)) {
            ruleClassifier.getConditionals().forEach((k, v) ->
                    this.query.append(updateColumnQuery(ruleClassifier.getTable(), ruleClassifier.getClassifier(), k, v)));
            return this;
        }
        else{
            ruleClassifier.getConditionals().forEach((k, v) ->
                    this.query.append(updateColumnCalcQuery(ruleClassifier.getTable(), ruleClassifier.getClassifier(), k)));
            return this;
        }
    }

    private String updateColumnQuery(String tableName, String columnName, String columnValue, DataClassifier conditionals){
        String updatequery = "UPDATE " + tableName + " SET " +  columnName + " = " + "\'" + columnValue + "\'" + " WHERE " + conditionals.toString() + ";";
        return updatequery;
    }

    private String updateColumnCalcQuery(String tableName, String columnName, String columnValue){
        String updatequery = "UPDATE " + tableName + " SET " +  columnName + " = " +  columnValue + ";";
        return updatequery;
    }



    public TableBuilder conditionalCombine(String logicalValue){
        this.query.append(" " + logicalValue.toUpperCase() + " ");
        return this;
    }

    public TableBuilder createTable(String tableName){
        this.query.insert(0,"CREATE TEMP TABLE " + tableName + " AS (");
        this.query.append(")");
        return this;
    }

    public TableBuilder selectTable(String tableName, List<String> columnNames){
        String cols = "";
        if(columnNames.size() == 0){
            cols = "*";
        }
        else {
            int i = 0;
            for (String col : columnNames) {
                if (i < columnNames.size() - 1) {
                    cols += col;
//                    cols += " AS ";
//                    cols += tableName;
//                    cols += "_";
//                    cols += col;
                    cols += ", ";
                }
                if (i == columnNames.size() - 1) {
                    cols += col;
//                    cols += " AS ";
//                    cols += tableName;
//                    cols += "_";
//                    cols += col;
                }
                i++;
            }
        }
        this.query.append("SELECT " + cols + " FROM " + tableName);
        return this;
    }

    public TableBuilder getTempTable(String tableName, List<String> columnNames){
        String cols = "";
        if(columnNames.size() == 0){
            cols = "*";
        }
        else {
            int i = 0;
            for (String col : columnNames) {
                if (i < columnNames.size() - 1) {
                    cols += col;
                    cols += ", ";
                }
                if (i == columnNames.size() - 1) {
                    cols += col;
                }
                i++;
            }
        }
        this.query.append("SELECT " + cols + " FROM " + tableName);
        return this;
    }

    public TableBuilder selectColumns(List<String> columns){
        String colString = "";
        int i = 0;
        for(String col :columns) {
            colString += col;
            if(i != columns.size() - 1) colString += ", ";
            i++;
        }
        this.query.insert(0,"SELECT " + colString + " FROM (");
        this.query.append(")");
        return this;
    }

    public TableBuilder addQuery(String query){
        this.query.insert(0,"SELECT " + query + " FROM (");
        this.query.append(") newTab");
        return this;
    }

}
