package org.cfi.rulebasedmodelling.postgres.datamanagement;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class TableCombination {

    private TableBuilder tableBuilder;
    private String columnAToJoinOn;
    private String columnBToJoinOn;
    private AtomicInteger numberOfJoins = new AtomicInteger();
    private Map<String, String> columnNames = Maps.newLinkedHashMap();

    private List<TableBuilder> tablesToJoin = Lists.newArrayList();
    private List<List<String>> columnsToJoinOn = Lists.newArrayList();
    private List<List<String>> selectColumns = Lists.newArrayList();
    private List<String> tableNames = Lists.newArrayList();
    private Map<Integer, String> tableNumbers = Maps.newLinkedHashMap();
    private List<String> trackColumnNames;

    public TableCombination(){
        this.tableBuilder = new TableBuilder();
    }

    public TableCombination addTable(TableBuilder tableBuilder, List<String> columnsToJoinOn, List<String> selectColumns, String tableName){
        this.tablesToJoin.add(tableBuilder);
        this.columnsToJoinOn.add(columnsToJoinOn);
        this.selectColumns.add(selectColumns);
        this.tableNames.add(tableName);
        return this;
    }

    public TableCombination createCombination(String tableName, List<String> commonNames){
        String prevTable = "";
        for(int i = 0; i < tablesToJoin.size(); i++){
            if(i == 0){
                prevTable = tablesToJoin.get(i).getQuery();
            }
            else{
                prevTable = insertJoin(prevTable, tablesToJoin.get(i).getQuery(), commonNames);
            }
        }
        this.tableBuilder = new TableBuilder(prevTable);
        return this;
    }

    private String insertJoin(String prevTab, String tab, List<String> commonNames){
        int tabNumA = numberOfJoins.getAndIncrement();
        int tabNumB = numberOfJoins.getAndIncrement();
        tableNumbers.put(tabNumA, tableNames.get(tabNumA));
        tableNumbers.put(tabNumB, tableNames.get(tabNumB));
        String joins = getJoinColumns(tabNumA,tabNumB,commonNames);
        String selects = getSelectColumns(tabNumA,tabNumB,commonNames);
        return "SELECT " + selects + " FROM ( (" + prevTab + ") tb" + tabNumA + " JOIN (" + tab + ") tb" +  tabNumB + joins + ")";
    }

    private String getJoinColumns(int prevTabNum, int tabNum, List<String> commonNames){
        List<String> colsA = prevTabNum == 0 ? columnsToJoinOn.get(prevTabNum) : commonNames;
        List<String> colsB = columnsToJoinOn.get(tabNum);
        StringBuilder sb = new StringBuilder(" ON (");
        for(int j = 0; j < colsA.size(); j++) {
            sb.append(" tb" + prevTabNum + ".");
            sb.append(colsA.get(j));
            sb.append(" = tb" + tabNum + ".");
            sb.append(colsB.get(j));
            if(j != colsA.size() - 1){
                sb.append(" AND ");
            }
            else{
                sb.append(")");
            }
        }
        return sb.toString();
    }

    private String getSelectColumns(int prevTabNum, int tabNum, List<String> commonNames){
        List<String> colsA = prevTabNum == 0 ? selectColumns.get(prevTabNum).stream().filter(c -> !columnsToJoinOn.get(prevTabNum).contains(c)).collect(Collectors.toList()) : trackColumnNames.stream().filter(c -> !commonNames.contains(c)).collect(Collectors.toList());
        List<String> colsB = selectColumns.get(tabNum).stream().filter(c -> !columnsToJoinOn.get(tabNum).contains(c)).collect(Collectors.toList());
        trackColumnNames = Lists.newArrayList();
        StringBuilder sb = new StringBuilder();
        for(String col : colsA){
            sb.append("tb" + prevTabNum + "." + col + ", ");
            trackColumnNames.add(col);
        }
        for(String col : colsB){
            if(trackColumnNames.contains(col)) {
                sb.append("tb" + tabNum + "." + col + " AS " + col + "_" + tabNum + ", ");
                trackColumnNames.add(col + "_" + tabNum);
            }
            else{
                sb.append("tb" + tabNum + "." + col + ", ");
                trackColumnNames.add(col);
            }
        }
        for(int i = 0; i < columnsToJoinOn.get(tabNum).size(); i++){
            sb.append("tb" + tabNum + "." + columnsToJoinOn.get(tabNum).get(i) + " AS " + commonNames.get(i) + ", ");
            trackColumnNames.add(commonNames.get(i));
        }
        sb.deleteCharAt(sb.length() - 2);
        return sb.toString();
    }

    public TableCombination addTable(TableBuilder tableBuilder, List<String> columnsToJoinOn, String commonName, int numberJoins){
        String columnToJoin = "";
        if(((numberOfJoins.get() & 1) == 0) && (numberOfJoins.get() != 0)){
            this.columnAToJoinOn = commonName;
        }
        if(numberOfJoins.get() == 0){
            createFirstTable(columnToJoin,tableBuilder);
            if(numberJoins == 1){
                handleOnlyOneTable(commonName);
            }
            return this;
        }
        else {
            int tblA = numberOfJoins.get();
            int tblB = numberOfJoins.getAndIncrement();
            wrapFirstTable(tblA,tblB,tableBuilder,commonName,columnToJoin);
            joinNextTable(tableBuilder,tblB);
            joinTablesOn(tblA,tblB);
            return this;
        }
    }

    private void createFirstTable(String columnToJoinOn, TableBuilder tableBuilder){
        this.tableBuilder.getBuilder().append(tableBuilder.getBuilder());
        this.columnAToJoinOn = columnToJoinOn;
        numberOfJoins.incrementAndGet();
        addToColumnNames(tableBuilder.getColumnNames(),-1);
    }

    private void handleOnlyOneTable(String commonName){
        int tblA = numberOfJoins.get();
        String select = generateColumns(tblA,commonName) + " FROM (";
        this.tableBuilder.getBuilder().insert(0,select);
        this.tableBuilder.getBuilder().append(") tb0");
    }

    private void wrapFirstTable(int tableA, int tableB, TableBuilder tableBuilder, String commonName, String columnToJoinOn){
        this.tableBuilder.getBuilder().append(") tb");
        this.tableBuilder.getBuilder().append((numberOfJoins.get() - 2));
        this.columnBToJoinOn = columnToJoinOn;

        addToColumnNames(tableBuilder.getColumnNames(), tableB);
        String select = generateColumns(tableA,commonName) + " FROM (";
        this.tableBuilder.getBuilder().insert(0,select);
    }

    private void joinNextTable(TableBuilder tableBuilder, int tableId){
        this.tableBuilder.getBuilder().append(" JOIN (");
        this.tableBuilder.getBuilder().append(tableBuilder.getBuilder());
        this.tableBuilder.getBuilder().append(") tb");
        this.tableBuilder.getBuilder().append(tableId);
    }

    private void joinTablesOn(int tableA, int tableB){
        this.tableBuilder.getBuilder().append(" ON tb");
        this.tableBuilder.getBuilder().append((tableA - 1));
        this.tableBuilder.getBuilder().append(".");
        this.tableBuilder.getBuilder().append(this.columnAToJoinOn);
        this.tableBuilder.getBuilder().append(" = tb");
        this.tableBuilder.getBuilder().append(tableB);
        this.tableBuilder.getBuilder().append(".");
        this.tableBuilder.getBuilder().append(this.columnBToJoinOn);
    }

//    public TableCombination joinTables(TableBuilder tableBuilderB, String columnBToJoinOn, String commonName){
//        this.columnBToJoinOn = columnBToJoinOn;
//        if(numberOfJoins.get() != 0) {
//            this.tableBuilder.getBuilder().append(")");
//            this.tableBuilder.getBuilder().append(" tb");
//            this.tableBuilder.getBuilder().append(numberOfJoins.get());
//        }
//        int tblA = numberOfJoins.getAndIncrement();
//        int tblB = numberOfJoins.getAndIncrement();
//
//        addToColumnNames(tableBuilderB.getColumnNames(), tblB);
//        String select = "SELECT tb" + tblA + "." + generateColumns(commonName) + " FROM (";
//
//        this.tableBuilder.getBuilder().insert(0,select);
//        this.tableBuilder.getBuilder().append(" JOIN (");
//        this.tableBuilder.getBuilder().append(tableBuilderB.getBuilder());
//        this.tableBuilder.getBuilder().append(") tb");
//        this.tableBuilder.getBuilder().append(tblB);
//        this.tableBuilder.getBuilder().append(" ON tb");
//        this.tableBuilder.getBuilder().append(tblA);
//        this.tableBuilder.getBuilder().append(".");
//        this.tableBuilder.getBuilder().append(this.columnAToJoinOn);
//        this.tableBuilder.getBuilder().append(" = tb");
//        this.tableBuilder.getBuilder().append(tblB);
//        this.tableBuilder.getBuilder().append(".");
//        this.tableBuilder.getBuilder().append(this.columnBToJoinOn);
//        return this;
//    }



//    public TableCombination(TableBuilder tableBuilder){
//        addToColumnNames(tableBuilder.getColumnNames(),-1);
//        this.tableBuilder = new TableBuilder();
//        this.tableBuilder.getBuilder().append(tableBuilder.getBuilder());
//        this.tableBuilder.getBuilder().append(") tb");
//        this.tableBuilder.getBuilder().append(numberOfJoins.get());
//    }

//    public TableCombination joinTables(TableBuilder tableBuilderB, String columnAToJoinOn, String columnBToJoinOn, String commonName){
//        this.columnAToJoinOn = columnAToJoinOn;
//        this.columnBToJoinOn = columnBToJoinOn;
//        if(numberOfJoins.get() != 0) {
//            this.tableBuilder.getBuilder().append(")");
//            this.tableBuilder.getBuilder().append(" tb");
//            this.tableBuilder.getBuilder().append(numberOfJoins.get());
//        }
//        int tblA = numberOfJoins.getAndIncrement();
//        int tblB = numberOfJoins.getAndIncrement();
//
//        addToColumnNames(tableBuilderB.getColumnNames(), tblB);
//        String select = "SELECT tb" + tblA + "." + generateColumns(commonName) + " FROM (";
//
//        this.tableBuilder.getBuilder().insert(0,select);
//        this.tableBuilder.getBuilder().append(" JOIN (");
//        this.tableBuilder.getBuilder().append(tableBuilderB.getBuilder());
//        this.tableBuilder.getBuilder().append(") tb");
//        this.tableBuilder.getBuilder().append(tblB);
//        this.tableBuilder.getBuilder().append(" ON tb");
//        this.tableBuilder.getBuilder().append(tblA);
//        this.tableBuilder.getBuilder().append(".");
//        this.tableBuilder.getBuilder().append(this.columnAToJoinOn);
//        this.tableBuilder.getBuilder().append(" = tb");
//        this.tableBuilder.getBuilder().append(tblB);
//        this.tableBuilder.getBuilder().append(".");
//        this.tableBuilder.getBuilder().append(this.columnBToJoinOn);
//        return this;
//    }

    public TableBuilder getTableBuilder(){
        return this.tableBuilder;
    }

//    private String generateColumns(String commonName){
//        if(this.columnNames.size()!= 0) {
//            String[] cN = commonName.split("\\.");
//            String function = this.columnNames.get(this.columnAToJoinOn);
//            if(cN.length == 2){
//                commonName = cN[0];
//                Matcher m = Pattern.compile("\\(([^)]+)\\)").matcher(cN[1]);
//                while(m.find()) {
//                    function = this.columnNames.get(this.columnAToJoinOn) + "+ interval " + "\'" + m.group(1) + "\'";
//                }
//            }
//            String cols = function + " AS " + commonName + ", ";
//            int i = 0;
//            for (String colName : this.columnNames.keySet()) {
//                if (!colName.equals(this.columnAToJoinOn) && !colName.equals(this.columnBToJoinOn)) {
//                    cols += this.columnNames.get(colName);
//                    cols += ", ";
//                }
//            }
//            cols = cols.substring(0,cols.length()-2);
//            return cols;
//        }
//        else
//            return "* ";
//    }

    private String generateColumns(int tableId, String commonName){
        if(this.columnNames.size()!= 0) {
            String[] cN = commonName.split("\\.");
            String function = "SELECT tb" + (tableId - 1) + "." + this.columnNames.get(this.columnAToJoinOn);
            if(cN.length == 2){
                commonName = cN[0];
                function = "SELECT date_trunc(" + "\'" + cN[1] + "\', " + "tb" + (tableId - 1) + "." + this.columnNames.get(this.columnAToJoinOn) + ")";
            }
            String cols = function + " AS " + commonName + ", ";
            int i = 0;
            for (String colName : this.columnNames.keySet()) {
                if (!colName.equals(this.columnAToJoinOn) && !colName.equals(this.columnBToJoinOn)) {
                    cols += this.columnNames.get(colName);
                    cols += ", ";
                }
            }
            cols = cols.substring(0,cols.length()-2);
            return cols;
        }
        else
            return "SELECT * ";
    }

    private void addToColumnNames(List<String> names, int tableNumber){
        names.stream().forEach(n -> {
            if (tableNumber != -1 && this.columnNames.keySet().contains(n)) {
                this.columnNames.put(n,"tb" + Integer.toString(tableNumber) + "." + n);
            } else {
                this.columnNames.put(n,n);
            }
        });
    }

}
