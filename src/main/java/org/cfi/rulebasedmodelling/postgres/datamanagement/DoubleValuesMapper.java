package org.cfi.rulebasedmodelling.postgres.datamanagement;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DoubleValuesMapper implements RowMapper<DoubleValues> {

    @Override
    public DoubleValues map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new DoubleValues()
                .setRowid(rs.getLong("rowid"))
                .setColumnid(rs.getLong("columnid"))
                .setValue(rs.getDouble("value"));
    }
}
