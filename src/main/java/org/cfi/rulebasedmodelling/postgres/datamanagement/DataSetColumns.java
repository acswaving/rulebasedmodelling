package org.cfi.rulebasedmodelling.postgres.datamanagement;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class DataSetColumns{

    @JsonProperty
    public long datasetid;                   // 1
    @JsonProperty
    public long columnid;                     // 1


    public DataSetColumns(){}

    public long getDatasetid() {
        return datasetid;
    }

    public DataSetColumns setDatasetid(long datasetid) {
        this.datasetid = datasetid;
        return this;
    }

    public long getColumnid() {
        return columnid;
    }

    public DataSetColumns setColumnid(long columnid) {
        this.columnid = columnid;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataSetColumns columns = (DataSetColumns) o;
        return datasetid == columns.datasetid;
    }

    @Override
    public int hashCode() {

        return Objects.hash(datasetid);
    }
}
