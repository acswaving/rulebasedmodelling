package org.cfi.rulebasedmodelling.postgres.datamanagement;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

public class DataSet{

    @JsonProperty
    public long id;                   // 1
    @JsonProperty
    public String name;               // acled
    @JsonProperty
    public DateTime last_updated;      // now()
    @JsonProperty
    public String description;        // armed conflict event data
    @JsonProperty
    public String source;             // www.acled.org/download


    public DataSet(){}


    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public DateTime getLast_updated() {
        return last_updated;
    }

    public String getDescription() {
        return description;
    }

    public String getSource() {
        return source;
    }

    public DataSet setId(long id) {
        this.id = id;
        return this;
    }

    public DataSet setName(String name) {
        this.name = name;
        return this;
    }

    public DataSet setLast_updated(DateTime last_updated) {
        this.last_updated = last_updated;
        return this;
    }

    public DataSet setDescription(String description) {
        this.description = description;
        return this;
    }

    public DataSet setSource(String source) {
        this.source = source;
        return this;
    }
}
