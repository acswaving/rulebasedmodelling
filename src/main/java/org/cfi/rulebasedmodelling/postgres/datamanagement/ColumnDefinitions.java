package org.cfi.rulebasedmodelling.postgres.datamanagement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ColumnDefinitions{

    @JsonProperty
    public long columnid;                   // 1
    @JsonProperty
    public String name;               // country
    @JsonProperty
    public String description;        // country name
    @JsonProperty
    public String type;             // StringValue
    @JsonProperty
    public long datasetid;             //lond


    public ColumnDefinitions(){}

    public long getColumnid() {
        return columnid;
    }

    public ColumnDefinitions setColumnid(long id) {
        this.columnid = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public ColumnDefinitions setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public ColumnDefinitions setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getType() {
        return type;
    }

    public ColumnDefinitions setType(String type) {
        this.type = type;
        return this;
    }

    public long getDatasetid() {
        return datasetid;
    }

    public ColumnDefinitions setDatasetid(long datasetid) {
        this.datasetid = datasetid;
        return this;
    }
}
