package org.cfi.rulebasedmodelling.postgres.datamanagement;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

public class DateTimeValues {

    @JsonProperty
    public long rowid;                   // 1
    @JsonProperty
    public long columnid;              // 1
    @JsonProperty
    public DateTime value;


    public DateTimeValues(){}

    public long getRowid() {
        return rowid;
    }

    public DateTimeValues setRow(long row) {
        this.rowid = row;
        return this;
    }

    public long getColumnid() {
        return columnid;
    }

    public DateTimeValues setColumnid(long columnid) {
        this.columnid = columnid;
        return this;
    }

    public DateTime getValue() {
        return value;
    }

    public DateTimeValues setValue(DateTime value) {
        this.value = value;
        return this;
    }
}
