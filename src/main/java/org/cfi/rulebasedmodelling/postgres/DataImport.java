package org.cfi.rulebasedmodelling.postgres;

import com.google.common.collect.Lists;
import org.cfi.rulebasedmodelling.api.Config;
import org.cfi.rulebasedmodelling.datamanagement.summaries.DataSummary;
import org.cfi.rulebasedmodelling.postgres.datamanagement.*;
import org.cfi.rulebasedmodelling.time.DateTimeUtil;
import org.cfi.rulebasedmodelling.util.LineParser;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class DataImport {
    private final Logger logger = LoggerFactory.getLogger(DataImport.class);

    private Config config;
    private final int batchSize = 500;

    public DataImport(Config config){
        this.config = config;
    }

    public void readInCsvBatch(String datasetName, String description, String source, String createdYear,
                               String updated, String whyCreated, String dataFile) throws IOException {
        readInCsvBatch(datasetName, description, source, createdYear, updated, whyCreated, new FileInputStream(dataFile));
    }

    public DataSummary readInCsvBatch(String datasetName, String description,
                                      String source, String createdYear,
                                      String updated, String whyCreated, InputStream inputStream) throws IOException {

        LineParser.ImportStats stats = new LineParser.ImportStats();

        config.dao.getLoadData().deleteExistingTable(datasetName);
        long maxDatasetId = getDatasetId();
        long maxColumnId = getMaxColumnId();

        String metaData = "{" + "\"description\":" + "\"" + description + "\"" + ",\"year created\":" + "\"" + createdYear + "\"" + ",\"last updated\":" + "\"" + updated + "\"" +
                ",\"purpose\":" + "\"" + whyCreated + "\"" + "}";

        DateTime dt = DateTime.now();
        DataSet ds = new DataSet()
                .setId(maxDatasetId+1)
                .setName(datasetName)
                .setLast_updated(dt)
                .setDescription(metaData)
                .setSource(source);
        this.config.dao.getLoadData().upsert(ds);

        AtomicInteger rowTracker = new AtomicInteger();
        List<String> columnTypes = Lists.newArrayList();

        List<DoubleValues> doublesBatch = new ArrayList<>(batchSize);
        List<StringValues> stringsBatch = new ArrayList<>(batchSize);
        List<DateTimeValues> datetimesBatch  = new ArrayList<>(batchSize);
        DataSummary dataSummary = new DataSummary(datasetName);
        //List<GeoPointValues> geoPointsBatch  = new ArrayList<>(batchSize);

        LineParser.csvRows(row -> {
            try {
                int rowIx = rowTracker.incrementAndGet();
                if(rowIx == 1) {
                    for (String c : row) {
                        columnTypes.add(c.substring(c.indexOf("#")+1));
                        dataSummary.initialseColumnData(c.substring(c.indexOf("#")+1));
                    }
                }
                else if(rowIx == 2) {
                    int colId = (int) maxColumnId + 1;
                    int iter = 0;
                    for (String c : row) {
                        writeColumnInformation(c,maxDatasetId+1,colId,columnTypes.get(iter));
                        dataSummary.setColumnName(c,iter);
                        colId++;
                        iter++;
                    }
                }
                else{

                    // collect values by type
                    int colId = (int) maxColumnId + 1;
                    int iter = 0;

                    for(String val : row) {
                        stats.totalLines.incrementAndGet();
                        String colType = columnTypes.get(iter);
                        switch (colType) {
                            case "numeric":
                                DoubleValues dv = new DoubleValues()
                                        .setColumnid(colId)
                                        .setRowid(rowIx);
                                if (val != null) {
                                    dv.setValue(Double.parseDouble(val));
                                }
                                doublesBatch.add(dv);
                                dataSummary.getColumns().get(iter).update(val);
                                break;
                            case "datetime":
                                DateTimeValues dtv = new DateTimeValues()
                                        .setColumnid(colId)
                                        .setRow(rowIx);
                                if (val != null) {
                                    dtv.setValue(datetimeParser(val));
                                }
                                datetimesBatch.add(dtv);
                                dataSummary.getColumns().get(iter).update(val);
                                break;
                            default:
                                StringValues sv = new StringValues()
                                        .setColumnid(colId)
                                        .setRowid(rowIx);
                                if (val != null) {
                                    sv.setValue(val);
                                }
                                stringsBatch.add(sv);
                                dataSummary.getColumns().get(iter).update(val);
                                break;
                        }
                        stats.validLines.incrementAndGet();

                        colId++;
                        iter++;
                    }
                }

                if(rowIx % batchSize == 0){
                    // once in a while, write the batch

                    this.config.dao.getLoadData().upsertStringValues(stringsBatch);
                    this.config.dao.getLoadData().upsertDateTimeValues(datetimesBatch);
                    this.config.dao.getLoadData().upsertDoubleValues(doublesBatch);
                    stringsBatch.clear();
                    datetimesBatch.clear();
                    doublesBatch.clear();
                    logger.info("Imported {} rows for {}", rowIx, datasetName);
                }
            } catch (Exception e) {
                System.out.println("ERROR row = " + e);
                System.out.println("row = " + rowTracker.get());
                stats.errorLines.incrementAndGet();
                stats.lastErrorMessage = e.getMessage();

            }
        }, inputStream);

        List<ColumnDefinitions> cds = this.config.dao.getReadData().datasetColumns(datasetName);
        for(ColumnDefinitions cd : cds){
            cd.setDescription(dataSummary.getColumnData(cd.getName()).toString());
            config.dao.getLoadData().upsertDescription(cd);
        }
        stats.finished.set(true);

        // write remaining batch values
        this.config.dao.getLoadData().upsertStringValues(stringsBatch);
        this.config.dao.getLoadData().upsertDateTimeValues(datetimesBatch);
        this.config.dao.getLoadData().upsertDoubleValues(doublesBatch);
        logger.info("Imported {} rows for {}", rowTracker.get(), datasetName);

        return dataSummary;

    }

    private void writeColumnInformation(String val, long id, long colId, String colType){
//            DataSetColumns dsc = new DataSetColumns()
//                    .setDatasetid(id)
//                    .setColumnid(colId);
//            this.config.dao.getLoadData().upsert(dsc);

            ColumnDefinitions cd = new ColumnDefinitions()
                    .setColumnid(colId)
                    .setName(val)
                    .setType(colType)
                    .setDatasetid(id);
            this.config.dao.getLoadData().upsert(cd);
    }

    private void writeCorrectType(String val, long colid, long rowCount, String colType){
        try {
            if (colType.equals("numeric")){
                DoubleValues dv = new DoubleValues()
                        .setColumnid(colid)
                        .setRowid(rowCount);
                if (val != null) {
                    dv.setValue(Double.parseDouble(val));
                }
                this.config.dao.getLoadData().upsert(dv);
            } else if(colType.equals("datetime")){
                DateTimeValues dt = new DateTimeValues()
                        .setColumnid(colid)
                        .setRow(rowCount);
                if (val != null) {
                        dt.setValue(datetimeParser(val));
                }
                this.config.dao.getLoadData().upsert(dt);
            }
            else {
                StringValues sv = new StringValues()
                        .setColumnid(colid)
                        .setRowid(rowCount);
                if (val != null) {
                    sv.setValue(val);
                }
                this.config.dao.getLoadData().upsert(sv);
            }
        }
        catch(Exception e){
            System.out.println("Exception: " + e );
            System.out.println("Row: " + rowCount);
            if (colType.equals("integer") || colType.equals("double")) {
                DoubleValues dv = new DoubleValues()
                        .setColumnid(colid)
                        .setRowid(rowCount);
                this.config.dao.getLoadData().upsert(dv);
            }
            else{
                StringValues sv = new StringValues()
                        .setColumnid(colid)
                        .setRowid(rowCount);
                this.config.dao.getLoadData().upsert(sv);
            }
        }
    }

    public DateTime datetimeParser(String datetime){
        /*DateTimeFormatter f = DateTimeFormat.forPattern("dd-MM-yyyy");
//        DateTimeFormatter f = DateTimeFormat.forPattern("dd-MMMM-yy");
        DateTime dateTime = f.parseDateTime(datetime);
        return dateTime;*/
        return DateTimeUtil.parseLiberalDateTime(datetime);
    }

    private long getDatasetId(){
        try{
            return config.dao.getReadData().getMaxDatasetId().get(0);

        }catch(Exception e){
            return 0;
        }
    }

    private long getMaxColumnId(){
        try{
            return config.dao.getReadData().getMaxColumnId().get(0);

        }catch(Exception e){
            return 0;
        }
    }


    public void writeModelInformation(String modelName, String columnsUsed,
                                      String columnTypes, String targetName,
                                      int targetIndex, String targetValues,
                                      String conditionalPaths,
                                      boolean useDateProperties){

        long maxDatasetId = getDatasetId();
        long maxColumnId = getMaxColumnId();
        DateTime dt = DateTime.now();

        if(!config.dao.getReadData().createdModelsExist()){

            DataSet ds = new DataSet()
                    .setId(maxDatasetId+1)
                    .setName("created_models")
                    .setLast_updated(dt)
                    .setDescription("Track created decision tree models")
                    .setSource("user");
            this.config.dao.getLoadData().upsert(ds);

            writeNewColumnDefinitions("model_name", maxColumnId, 1, maxDatasetId,
                    "Name of Model Saved", "string");
            writeNewColumnDefinitions("column_names", maxColumnId, 2, maxDatasetId,
                    "Columns used to predict feature", "string");
            writeNewColumnDefinitions("column_types", maxColumnId, 3, maxDatasetId,
                    "Column types defined from training set", "string");
            writeNewColumnDefinitions("target_name", maxColumnId, 4, maxDatasetId,
                    "Column name of feature trying to be predicted", "string");
            writeNewColumnDefinitions("target_index", maxColumnId, 5, maxDatasetId,
                    "Column index of target", "string");
            writeNewColumnDefinitions("target_values", maxColumnId, 6, maxDatasetId,
                    "Values of target value", "string");
            writeNewColumnDefinitions("conditional_paths", maxColumnId, 7, maxDatasetId,
                    "All paths for each label", "string");
            writeNewColumnDefinitions("use_date_properties", maxColumnId, 8, maxDatasetId,
                    "Model uses date properties (e.g. what week of year is it in)", "string");

        }

        if(config.dao.getReadData().createdModelExist(modelName)){
            config.dao.getLoadData().deleteCreatedModel(modelName);
        }

        long modelNameid = getColumnId("created_models", "model_name");
        long colNamesid = getColumnId("created_models", "column_names");
        long colTypesid = getColumnId("created_models", "column_types");
        long targetNameid = getColumnId("created_models", "target_name");
        long targetIndexid = getColumnId("created_models", "target_index");
        long targetValuesid = getColumnId("created_models", "target_values");
        long conditionalPathsid = getColumnId("created_models", "conditional_paths");
        long datePropertiesid = getColumnId("created_models", "use_date_properties");

        int maxRowId = this.config.dao.getReadData().getMaxRowId("created_models", "stringvalues");

        writeStringToTable(modelName, modelNameid,maxRowId);
        writeStringToTable(columnsUsed, colNamesid, maxRowId);
        writeStringToTable(columnTypes, colTypesid, maxRowId);
        writeStringToTable(targetName, targetNameid, maxRowId);
        writeStringToTable(Integer.toString(targetIndex), targetIndexid, maxRowId);
        writeStringToTable(targetValues, targetValuesid, maxRowId);
        writeStringToTable(conditionalPaths, conditionalPathsid, maxRowId);
        writeStringToTable(Boolean.toString(useDateProperties), datePropertiesid, maxRowId);

    }

    private void writeNewColumnDefinitions(String columnName,
                                           long maxColumnId,
                                           int idIncrement,
                                           long maxDatasetId,
                                           String description,
                                           String columnType){
        ColumnDefinitions cd = new ColumnDefinitions()
                .setColumnid((int) maxColumnId + idIncrement)
                .setName(columnName)
                .setDescription(description)
                .setType(columnType)
                .setDatasetid(maxDatasetId+1);
        this.config.dao.getLoadData().upsert(cd);
    }

    private long getColumnId(String datsetName, String columnName){
        return this.config.dao.getReadData().datasetColumns(datsetName)
                .stream().filter(c -> c.getName().equals(columnName))
                .collect(Collectors.toList()).get(0).getColumnid();
    }

    private void writeStringToTable(String columnName, long columnId, int maxRowId){
        StringValues tv = new StringValues()
                .setColumnid(columnId)
                .setRowid(maxRowId + 1);
        if (columnName != null) {
            tv.setValue(columnName);
        }
        this.config.dao.getLoadData().upsert(tv);
    }

}
