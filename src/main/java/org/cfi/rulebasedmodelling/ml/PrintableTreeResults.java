package org.cfi.rulebasedmodelling.ml;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Maps;

import java.util.Map;

public class PrintableTreeResults {

    @JsonProperty
    private String feature;
    @JsonProperty
    private String label;
    @JsonProperty
    private String accuracy;
    @JsonProperty
    private String error;
    @JsonProperty
    private String precision;
    @JsonProperty
    private String recall;
    @JsonProperty
    private Map<String, String> featureImportance = Maps.newLinkedHashMap();
    @JsonProperty
    private String baselineAccuracy;

    public PrintableTreeResults(){}

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }

    public void setError(String error) {
        this.error = error;
    }

    public void setPrecision(String precision) {
        this.precision = precision;
    }

    public void setRecall(String recall) {
        this.recall = recall;
    }

    public void setFeatureImportance(Map<String, String> featureImportance) {
        this.featureImportance.putAll(featureImportance);
    }

    public void setBaselineAccuracy(String accuracy) {
        this.baselineAccuracy = accuracy;
    }

    public String getFeature() {
        return feature;
    }

    public String getLabel() {
        return label;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public String getError() {
        return error;
    }

    public String getPrecision() {
        return precision;
    }

    public String getRecall() {
        return recall;
    }

    public Map<String, String> getFeatureImportance() {
        return featureImportance;
    }
}
