package org.cfi.rulebasedmodelling.ml;

import weka.classifiers.trees.J48;

/**
 * Extend J48 decision tree, to allow for batch processing.
 * @author Aaron Swaving
 */

public class J48Batch extends J48 {

    @Override
    public boolean implementsMoreEfficientBatchPrediction() {
        return true;
    }
}
