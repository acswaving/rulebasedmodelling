package org.cfi.rulebasedmodelling.time;

import org.joda.time.DateTime;

/**
 * An object with a DateTime label
 * @version 30-1-17
 * @author Arvid Halma
 */
public interface TimeStamped {
    DateTime getTimeStamp();
}
