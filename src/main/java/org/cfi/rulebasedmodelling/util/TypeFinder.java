package org.cfi.rulebasedmodelling.util;

public class TypeFinder {

    public TypeFinder(){
    }

    public boolean isNumber(String input) {
        if(input.chars().allMatch(x -> Character.isDigit(x))){return true;}

        String[] decimal = input.split(".");
        if(decimal.length == 2
                && (decimal[0].chars().allMatch(x -> Character.isDigit(x))
                && decimal[1].chars().allMatch(x -> Character.isDigit(x)))){return true;}

                return false;
    }

    public boolean isDateTime(String input){

        return false;
    }

}
