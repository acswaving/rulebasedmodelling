package org.cfi.rulebasedmodelling.util;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Pattern;

/**
 * Iterate and process each line in a (list of) input streams.
 * Includes CSV parser.
 * @author Arvid
 * @version 13-10-2015 - 21:07
 */
public class LineParser {

    public static void lines(Consumer<String> lineConsumer, File file) throws IOException {
        lines(lineConsumer, file, "UTF-8");
    }

    public static void lines(Consumer<String> lineConsumer, File file, String encoding) throws IOException {
        lines(lineConsumer, new FileInputStream(file), encoding, new ImportStats());
    }

    public static void lines(Consumer<String> lineConsumer, InputStream is, String encoding, ImportStats stats) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(is, encoding))) {
            String line;
            while ((line = br.readLine()) != null) {
                stats.totalLines.incrementAndGet();
                // line = line.trim();
                if (!line.isEmpty() && !line.startsWith("#")) {
                    stats.dataLines.incrementAndGet();
                    // lines line
                    lineConsumer.accept(line);
                    stats.validLines.incrementAndGet(); // no exception
                }
            }
        }
    }

    public static Iterator<String> linesIter(String encoding, InputStream is) throws IOException {
        return new RowIterator(new BufferedReader(new InputStreamReader(is, encoding)), new ImportStats());
    }

    public static void tsvRows(Consumer<String[]> rowConsumer, File file) throws IOException {
        csvRows("[\t]", true, rowConsumer, "UTF-8", true, file);
    }

    public static void tsvRows(Consumer<String[]> rowConsumer, InputStream is) throws IOException {
        csvRows("[\t]", true, rowConsumer, "UTF-8", true, is);
    }

    public static RecordIterator<String[]> tsvRecordIterator(InputStream is) throws IOException {
        return new RecordIterator<>(is, vals -> vals, new ImportStats());
    }

    public static void csvRows(Consumer<String[]> rowConsumer, File file) throws IOException {
        csvRows("[,]", true, rowConsumer, "UTF-8", true, file);
    }

    public static void csvRows(Consumer<String[]> rowConsumer, InputStream is) throws IOException {
        csvRows("[,]", true, rowConsumer, "UTF-8", true, is);
    }

    public static void csvRows(String separator, boolean unquote, Consumer<String[]> rowConsumer, String encoding, boolean emptyStringAsNull, File file) throws IOException {
        lines(columnConsumer(unquote, rowConsumer, emptyStringAsNull, separator), file, encoding);
    }

    public static void csvRows(String separator, boolean unquote, Consumer<String[]> rowConsumer, String encoding, boolean emptyStringAsNull, InputStream is) throws IOException {
        lines(columnConsumer(unquote, rowConsumer, emptyStringAsNull, separator), is, encoding, new ImportStats());
    }

    private static Consumer<String> columnConsumer(boolean unquote, Consumer<String[]> rowConsumer, boolean emptyStringAsNull, String separator) {
        Splitter splitter = Splitter.on(Pattern.compile(separator+"(?=([^\"]*\"[^\"]*\")*[^\"]*$)"));
        return line -> {
            String[] vals = Iterables.toArray(splitter.split(line), String.class);
            if(unquote) {
                for (int i = 0; i < vals.length; i++) {
                    String v = unquote(vals[i].trim());
                    vals[i] = emptyStringAsNull && v.isEmpty() ? null : v;
                }
                rowConsumer.accept(vals);
            }
        };
    }

    public static void convert(File in, File out, String separator, Function<String[], String[]> f) throws IOException {
        RowWriter rowWriter = new RowWriter(out, separator);
        csvRows(separator, true, row -> rowWriter.accept(f.apply(row)), "UTF-8", true, in);
        rowWriter.close();
    }


    /**
     * This String util method removes single or double quotes
     * from a string if its quoted.
     * for input string = "mystr1" output will be = mystr1
     * for input string = 'mystr2' output will be = mystr2
     *
     * @param s to be unquoted.
     * @return value unquoted, null if input is null.
     *
     */
    public static String unquote(String s) {
        if (s != null && s.length() >= 2
                && ((s.startsWith("\"") && s.endsWith("\""))
                || (s.startsWith("'") && s.endsWith("'"))))
        {
            s = s.substring(1, s.length() - 1);
        }
        return s;
    }

    public static double parseSloppyDouble(String s){
        if(s == null){
            return Double.NaN;
        }
        try {
            return Double.parseDouble(s.replace(',', '.'));
        } catch(NumberFormatException ignored) {
            return Double.NaN;
        }
    }

    public static Double parseSloppyDoubleObject(String s){
        if(s == null){
            return null;
        }
        try {
            return Double.parseDouble(s.replace(',', '.'));
        } catch(NumberFormatException ignored) {
            return null;
        }
    }

    public static class RowWriter implements Consumer<String[]> {
        File outFile;
        String separator;

        BufferedWriter writer;

        public RowWriter(File outFile, String separator) throws IOException {
            this.outFile = outFile;
            this.separator = separator;
            writer = new BufferedWriter(new FileWriter(outFile));
        }

        @Override
        public void accept(String[] vals) {
            if(vals != null && vals.length > 0) {
                StringJoiner joiner = new StringJoiner(separator);
                for (String val : vals) {
                    joiner.add(val);
                }
                try {
                    writer.write(joiner.toString());
                    writer.write('\n');
                } catch (IOException ignored) {
                }
            }
        }

        public void close() throws IOException {
            writer.close();
        }
    }


    /**
     * Line iterator that skips emty or commented lines.
     */
    public static class RowIterator extends LineIterator<String> {


        public RowIterator(Reader reader, ImportStats stats) throws IllegalArgumentException {
            super(reader, s -> s, stats);
        }

        @Override
        protected boolean isValidLine(String line) {
            stats.totalLines.incrementAndGet();
            boolean valid = line != null && !line.isEmpty() && !line.startsWith("#");
            if(valid){
                stats.validLines.incrementAndGet();
                stats.dataLines.incrementAndGet();
            }
            return valid;
        }
    }


    /**
     * Line iterator that yields pojos, given a function that transforms Stringp[] to the pojo.
     * @param <T> the object type to yield.
     */
    public static class RecordIterator<T> extends LineIterator<T> {
        Function<String[], T> recordMapper;
        Consumer<String> onRecordError;
        ImportStats stats;

        public RecordIterator(InputStream is, Function<String[], T> recordMapper, ImportStats stats) throws IllegalArgumentException {
            this(new BufferedReader(new InputStreamReader(is)), Splitter.on(Pattern.compile("[\t](?=([^\"]*\"[^\"]*\")*[^\"]*$)")), recordMapper, s -> {}, stats, true, true);
            this.recordMapper = recordMapper;
        }

        public RecordIterator(
                Reader reader,
                Splitter splitter,
                Function<String[], T> recordMapper,
                Consumer<String> onRecordError,
                ImportStats stats,
                boolean unquote,
                boolean emptyStringAsNull
        ) throws IllegalArgumentException {
            super(reader, line -> {
                String[] vals = Iterables.toArray(splitter.split(line), String.class);
                for (int i = 0; i < vals.length; i++) {
                    String v = unquote ? unquote(vals[i]) : vals[i];
                    vals[i] = emptyStringAsNull && v.isEmpty() ? null : v;
                }
                return recordMapper.apply(vals);
            }, stats);
            this.recordMapper = recordMapper;
            this.onRecordError = onRecordError;
            this.stats = stats;
        }

        /*public static RecordIterator<String[]> recordIterator(Reader reader, boolean unquote, boolean emptyStringAsNull) throws IllegalArgumentException {
            return recordIterator(reader, "[\t]", unquote, emptyStringAsNull);
        }

        public static RecordIterator<String[]> recordIterator(Reader reader, String splitter,  boolean unquote, boolean emptyStringAsNull) throws IllegalArgumentException {
            return new RecordIterator<>(reader, Splitter.on(Pattern.compile(splitter)), line -> line, line -> {}, new ImportStats(), unquote, emptyStringAsNull);
        }*/

        @Override
        protected boolean isValidLine(String line) {
            boolean saneLine = line != null && !line.isEmpty() && !line.startsWith("#");
            stats.totalLines.incrementAndGet();
            if(!saneLine){
                return false;
            }
            try{
                stats.dataLines.incrementAndGet();
                cachedObject = lineConverter.apply(line);
                stats.validLines.incrementAndGet();
            } catch (Exception e){
                cachedObject = null;
                stats.errorLines.incrementAndGet();
                stats.lastErrorMessage = e.getMessage();
                onRecordError.accept(line);
                return false;
            }
            return true;
        }
    }


    /** Generalized from: org.apache.commons.io.LineIterator */
    public static class LineIterator<T> implements Iterator<T> {
        private final BufferedReader bufferedReader;
        private String cachedLine;
        protected T cachedObject;
        private boolean finished = false;
        protected Function<String, T> lineConverter;
        protected ImportStats stats;

        public LineIterator(Reader reader, Function<String, T> lineConverter, ImportStats stats) throws IllegalArgumentException {
            this.lineConverter = lineConverter;
            this.stats = stats;
            if(reader == null) {
                throw new IllegalArgumentException("Reader must not be null");
            } else {
                if(reader instanceof BufferedReader) {
                    this.bufferedReader = (BufferedReader)reader;
                } else {
                    this.bufferedReader = new BufferedReader(reader);
                }

            }
        }

        public boolean hasNext() {
            if(stats.cancel.get()){
                return false;
            }
            if(this.cachedLine != null) {
                return true;
            } else if(this.finished) {
                return false;
            } else {
                try {
                    String ioe;
                    do {
                        ioe = this.bufferedReader.readLine();
                        if(ioe == null) {
                            this.finished = true;
                            return false;
                        }
                    } while(!this.isValidLine(ioe));

                    this.cachedLine = ioe;
                    return true;
                } catch (IOException var2) {
                    this.close();
                    throw new IllegalStateException(var2);
                }
            }
        }

        protected boolean isValidLine(String line) {
            return true;
        }

        public T next() {
            stats.totalLines.incrementAndGet();
            return this.nextLine();
        }

        public T nextLine() {
            if(!this.hasNext()) {
                throw new NoSuchElementException("No more lines");
            } else {
                this.cachedLine = null;
                return cachedObject;
            }
        }

        public void close() {
            this.finished = true;
            IOUtils.closeQuietly(this.bufferedReader);
            this.cachedLine = null;
        }

        public void remove() {
            throw new UnsupportedOperationException("Remove unsupported on LineIterator");
        }

        public static void closeQuietly(LineIterator iterator) {
            if(iterator != null) {
                iterator.close();
            }

        }
    }

    /**
     * Keep track of upload progress
     * @author Arvid Halma
     * @version 15-3-2017 - 15:53
     */
    public static class ImportStats {
        @JsonProperty
        public AtomicInteger totalLines;

        @JsonProperty
        public AtomicInteger dataLines;

        @JsonProperty
        public AtomicInteger validLines;

        @JsonProperty
        public AtomicInteger errorLines;

        @JsonProperty
        public String lastErrorMessage;

        @JsonProperty
        public List<String> messages;


        @JsonProperty
        public AtomicBoolean cancel;

        @JsonProperty
        public AtomicBoolean finished;

        public ImportStats() {
            totalLines = new AtomicInteger();
            dataLines = new AtomicInteger();
            validLines =  new AtomicInteger();
            errorLines = new AtomicInteger();

            messages = new ArrayList<>();

            cancel = new AtomicBoolean(false);
            finished = new AtomicBoolean(false);

        }

        @Override
        public String toString() {
            return "ImportStats{" +
                    "totalLines=" + totalLines +
                    ", dataLines=" + dataLines +
                    ", validLines=" + validLines +
                    ", errorLines=" + errorLines +
                    ", lastErrorMessage='" + lastErrorMessage + '\'' +
                    ", messages=" + messages +
                    ", cancel=" + cancel +
                    ", finished=" + finished +
                    '}';
        }
    }

}
